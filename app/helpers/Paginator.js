import React from 'react';

import './Paginator.less';

export class Paginator2 extends React.Component
{
	constructor(props)
	{
		super(props);
		this.onClick = this.onClick.bind(this);
		this.children = this.children.bind(this);
		this.render = this.render.bind(this);
	}
	
	onClick(index)
	{
		if(this.props.onClick)
			this.props.onClick(index);
	}
	
	children()
	{
		var arr = [];
		for(let i = 0; i < this.props.pages; i++)
		{
			arr.push(
				<a className={`button ${this.props.currentIndex === i? "current" : ""}`}
				   onClick={(e) => this.onClick(i)}>
					<div className="circle"/>
				</a>
			);
		}
		return arr;
	}
	
	render = () =>
		<div className="paginator2">
		{this.children()}
		<a className={`button mover moving${this.props.currentIndex}`}>
			<div className="dot"/>
		</a>
		</div>;
	
}

export class Paginator extends React.Component
{
	constructor(props)
	{
		super(props);
		this.children = this.children.bind(this);
		this.render = this.render.bind(this);
	}
	
	children()
	{
		var arr = [];
		for(var i =0; i < this.props.pages; i++)
			arr.push(
				<a className={`button ${this.props.currentIndex === i? "current" : ""} ${i < this.props.currentIndex? `before${this.props.currentIndex % 2}` : ""}`}>
					<div className="dot">
						<div className="dotLeft"/>
						<div className="dotMiddle"/>
						<div className="dotRight"/>
					
					</div>
				</a>
			);
		return arr;
	}
	
	render = () =>
		<div className="paginator">
		{this.children()}
		</div>;
}

export default Paginator;