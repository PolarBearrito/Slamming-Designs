import React from 'react';
const RRedux = require('react-redux');

import { enterMainPage } from '../redux/reducers/IntroReducer';

	
	
var Intro = React.createClass(
{
	render: function()
	{
		return (
			<div className="fullOverlay clickable" ref={ thisElm => { this.overlay = thisElm} } 
				onClick={ e => 
							{
								this.logo.src = "/img/intro-gif.gif";
								window.setTimeout(() => this.props.setEntered(), 2500);
								window.setTimeout(() => this.overlay.className += " gone", 2000);
							}
						}>
				<img src="/img/intro-static.png" className="introLogo"				
					 ref={ thisElm => { this.logo = thisElm; } }
					 />
				<div className="introText">
					Full Stack Developer + UI/UX Designer
				</div>
			</div>
		);
	}
});
Intro = RRedux.connect(
	
	(state, ownProps) => 
	({
		location: ownProps.location || 0
	}),
	
	d =>
	({
		d: d
	}),
	
	(s, {d}) =>
	({
		setEntered()
		{
			if(location)
				d(enterMainPage(location.pathname+location.hash));
		}		
	})
	
)(Intro);



	
export default Intro;