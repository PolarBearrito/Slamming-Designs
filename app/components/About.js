import React from 'react';
const RRedux = require('react-redux');

import { PAGES, changePage, changePageRoute } from '../redux/reducers/NavigationReducer';

import PageTitle from './PageTitle';

import { u } from "../helpers/toUnicode";

import {P, UL} from "./projects/ContentComponents";

import SubHeading from './projects/SubHeading';

import { setAboutFAQs, setAboutCurrFAQsIdx } from '../redux/reducers/AboutReducer';


class FAQLink extends React.Component
{
	render()
	{
		return(<a key={this.props.name} href={`#${this.props.name}`}>{this.props.title}</a>);
	}
}

class FAQsContent extends React.Component
{
	componentWillMount()
	{
		var FAQs = [];
		var index = 0;
		var newProps = {};
		
		var newChildren = React.Children.map
		(
			this.props.children,
			child =>
			{
				if(child.type && 
				   child.type.displayName &&
				   child.type.displayName === SubHeading.displayName)
				{
					FAQs[index] = 
					{
						name: 	child.props.name,
						title:	child.props.title
					}
					
					newProps = Object.assign({}, child.props, 
						{
							index:  index
						}
					);
					index++;
					return React.cloneElement(child, newProps);
				}
				return child;
			}
		)
		
		this.props.setFAQs(FAQs);
	}	
	
	render()
	{
		return(
		<div className="FAQsContent page narrative narrativeText" ref={thisElm => this.FAQsContent = thisElm}>
			{this.props.children}
			

			</div>
		);
	}
}
FAQsContent = RRedux.connect(
	
	(s, ownProps) => 
	({	
		location: ownProps.location
	}),
	
	d =>
	({
		setFAQs(FAQs) { d(setAboutFAQs(FAQs)) },
		setFAQ(index) { d(setAboutCurrFAQsIdx(index)); }
	})
	
)(FAQsContent);

import './NavButtons.less';
import './About.less';

export var About = React.createClass
({	
	
	componentWillMount()
	{
		const props = this.props;
		
		props.setPageAbout();
	},
	
	render()
	{
		const props = this.props;
		
		window.locationX = props.location;

		return(
		<div className="aboutContainer">
			<PageTitle title={"About"}/>
			
			<div className="navButtons">
				<div className="toTop clickable" 
					onClick={()=>{
					$('html, body').animate(
					{ scrollTop: 0}, 500);
					}}>{u("f062")}</div>
				<div className="toFAQs clickable" 
					onClick={()=>{
					$('html, body').animate(
					{ scrollTop: $("#FAQs").offset().top}, 500);
					}}>FAQs</div>
			</div>
			
			<div className="aboutHero">
				<div className="aboutWhiteCover">
					<div/>
				</div>
				<div className="page">
					<div className="aboutText">
						<div className="aboutHead">
							Hi!
						</div>
						<div className="aboutSubhead">
							I'm Steven 'Slam' Lam <br/>
							and welcome to my site.
						</div>
					</div>
					<div className="aboutPC">
						Photo Credit: Shannon Li
					</div>
				</div>
			</div>
			
			<div className="page aboutIntro narrative narrativeText">
				<P>
					<b>This is a work in progress.</b>
				</P>
				<P>
					<a className="clickable" onClick={props.setPagePortfolioRoute}>Explore my portfolio</a>
				</P>
				<P>
					<b>Almost everything on the site</b> from the illustrations, to the code, to the design is done by me. The site runs on React.js Redux.
				</P>
				<P>
					This is an old site and hasn't been updated in a while.
				</P>
				<h2>
					How I organized my portfolio:
				</h2>
				<P>
					Rather than the typical gallery dump, or scrolljacking slideshow, I've organized my portfolio as a set of case studies instead. I feel there are a few benefits to this:
				</P>
				<UL>
					<li>1. I can clearly talk about what <i>I</i> did exactly</li>
					<li>2. I can explain my thought process for the decisions I made</li>
					<li>3. I can showcase my philosophies when it comes to design, architecture, etc.
					</li>
				</UL>
				<P>
					I hope that my efforts will be appreciated. Also, however, the work I do at my current job doesn't produce any public/consumer facing results, so there isn't much I can show you from what I do professionally.
				</P>
				<h2>
					How this page will be organized:
				</h2>
				<P>
					I have decided to write about myself in the form of a FAQ, a sort of self interview, to keep things easy to find, but also very in-depth.
				</P>
			
			</div>
			
			<div id="FAQs" className="FAQs">
			
				<div className="page">
			
					<div className="largeHeading">
						FAQs
					</div>
					<div className="FAQsQuestions">
						{this.props.FAQs.map(FAQ => <FAQLink 
						key={FAQ.name} 
						name={FAQ.name}
						title={FAQ.title}/>)}
					</div>
				</div>
			
			
			</div>
			
			<FAQsContent>
				<SubHeading title="What is your academic background?" name="academic-background"/>
				<P>
					In high school I majored in computer science (yes, my high school had majors). I took classes on web development, Java, A+, and Oracle database modelling. 
				</P>
				<P>
					I have a bachelors of science in Computer and Electrical Engineering from the NYU Tandon School of Engineering with a minor in Psychology.
				</P>
				
				<SubHeading title="How would you identify yourself professionally?" name="professional-identity"/>
				<P>
					I would say that I am a Senior Frontend Engineer.
				</P>
				
				<SubHeading title="What are you looking for professionally?" name="looking-professionally"/>
				<P>
					In short, I'm looking for something fulfilling while also giving me opportunities to learn and grow more. Specifically, I've spent a lot of time thinking about how to solve foundational problems in the frontend/UI/UX domain. One fundamental problem is building a design system that is rigid and consistent but flexible enough to support new patterns
				</P>
				<P>
					I've come up with a few developer experience-friendly solutions in a few spaces that I've field tested. They balance consistency with flexibility. Common UX patterns are ingrained into the system but have some defined escape hatches to define new patterns.
				</P>
				<P>
					Being able to share some of my expertise while continuing to solve foundational problems, and growing to be more well rounded (such as developing more on the BE side) is one of my goals. Other goals is to explore less common technologies like websockets and real time updates.
				</P>
				<P>
					In the long term, I have a lot of entrepreneurial pursuits that are mostly consumer facing. So being able to learn more in that domain is definitely a goal. I want to learn the ins and outs of producing a product that caters to a specific market, projects where we are allowed to do things like market analysis, user analysis, and data-driven design.
				</P>
				<P>
					I want to see the role my design and psychology backgrounds play in producing a consumer grade software. To me, consumers are the most interesting and potentially predictable market.
				</P>
				
				<SubHeading title="why do you have such a broad skill set? Are you disorganized?" name="why-broad-disorganized"/>
				
				<P>
					Every one of my skills serves a purpose and I am passionate about every one of them. Eventually I want to be able to design, prototype, build, and code a biomedical hardware device and the associated app from start to finish. Then, I want to market and sell this product. 
				</P>
				<P>
					So to achieve this dream, I need a broad skill set. The dreams that I have drive me to gain the skills I need to realize my dreams. 
				</P>
				
				
				<SubHeading title="How did you start coding? How did you start designing? How did you get such a broad skill set?" name="start-coding"/>
				<P>
					I started doing coding and design work when I was pretty young.
				</P>
				<P>
					When I was around 11, someone on an online game showed me how to code an auto-clicker. At the time it was a completely foreign concept to me, but it was very interesting, how a few lines of code could produce something so useful.
				</P>
				<P>
					I played around with that software, AutoHotkey, for years. I started studying the extremely detailed help file and I began to code macros and show them to my friends. It was so fun and useful. I created macros to help me cheat in games, reduce my workload on certain tasks, and other really cool stuff. 
				</P>
				<P>
					For example, I have a TV mounted on the side of my wall that I play videos on to create background noise. I have to turn my head 130 degrees backwards in order to control what's happening on the mounted TV. That was very annoying, so I created a script that would detect when I moved my mouse onto the TV monitor and display a preview of what I'm doing on that screen on my monitor on my desk.
				</P>
				<P>
					After learning to code, ideas like that started coming to me all the time. I realized the possibilities were endless. I was only in middle school at the time and I knew that programming could lead me to financial independence. I looked to Bill Gates as a role model.
				</P>
				<P>
					I started to pursue other goals, such as video game development. I had an idea for a game and I really wanted to pursue it so I started learning python, and played around with pygame.
				</P>
				<P>
					I was also very interested in art at the time and I really liked pixel art, so for a while I did a bunch of that. I pretty good at using photoshop at this point.
				</P>
				<P>
					When I went into high school, by majoring in computer science I started learning how to develop dynamic web pages. I started to combine my art skills and passions with code and really focused on front end development for a bit. I tried to push CSS3 to it's limits, doing things in CSS3 what might typically be done in javascript.
				</P>
				<P>
					Over time my ideas expanded and started to include hardware, so going into college I majored in computer and electrical engineering. This really pushed my knowledge of computer science all the way down to the quantum level, it was really cool.
				</P>
				<P>
					During college I really tried to pursue the development of my ideas, and I started becoming quite professional with design work when I started to design my own app. I trained myself in material design and learned how to use illustrator and after effects to produce mock-ups that I would eventually code.
				</P>
				<P>
					In college I was able to get a job doing software professionally. I was the only one in the team that knew how to design, and so I became the lead designer. I refined my design abilities by tackling the multitude of problems that our clients would submit to us. At the same time, because my designs were usually complex, I would have to be the one to code it.
				</P>
				<P>
					Over time, by pursuing avenues where I utilize my skills, I managed to land where I am now.
				</P>

				<SubHeading title="Why did you minor in psychology?" name="minor-psychology"/>
				<P>
					3 reasons:
				</P>
				<UL>
					<li>
					1. Psychology plays a huge part in design as it studies information processing and cognition. In fact, in the human-computer interface course I took, we studied a book, Persuasive Technology, and being trained in psychology I recognized that a lot of the concepts in the book were from psychology.
					</li>
					<li>
					2. Psychology also plays a huge role in marketing, user analysis, etc., basically anything that involves people. So it would be equally helping in product design and the business aspects of a project.
					</li>
					<li>
					3. Personally, I will eventually pursue medicine, and psychiatry and abnormal psychology is one of the most interesting fields to me. The brain is so poorly understood that that field of study is a gold mine waiting to be explored. 
					</li>
				</UL>
				
				<SubHeading title="what are the benefits of having such a broad skill set?" name="benefits-broad-skill-set"/>
				
				<P>
					It shortens the development lifecycle basically. You can self-validate a lot of your own ideas and projects. It's only when your project scales that you need to include specialists to push that domain further.
				</P>
			
			</FAQsContent>
			
			
			<div className="emptySpace">&nbsp;</div>
			<div className="emptySpace">&nbsp;</div>
			
		</div>
		);
	}
});
About = RRedux.connect(
	
	(state, ownProps) => 
	({
		FAQs: state.About.FAQs,
	}),
	
	d => 
	({	
		setPageAbout() 				{ d(changePage(PAGES.ABOUT)) },
		setPagePortfolioRoute()		{ d(changePageRoute(PAGES.PORTFOLIO)) }
	})
	
)(About);

		


	
export default About;