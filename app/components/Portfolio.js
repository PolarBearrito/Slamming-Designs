import React from 'react';
const RRedux = require('react-redux');
import { CATEGORIES, changeCategoryRoute, changeCategory, NAMES } from '../redux/reducers/PortfolioReducer';

import { PAGES, changePage } from '../redux/reducers/NavigationReducer';
import { setProject, setProjectRoute } from '../redux/reducers/ProjectReducer';

import { PROJECTS } from '../redux/reducers/Projects';

import Project from './projects/Project';

import PageTitle from './PageTitle';

import { u } from "../helpers/toUnicode";

var MediaQuery = require('react-responsive');

import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import ReactDOM from 'react-dom'

import { Paginator } from '../helpers/Paginator';

// https://tc39.github.io/ecma262/#sec-array.prototype.findIndex
if (!Array.prototype.findIndex) 
{
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return -1.
      return -1;
    }
  });
}

var icons = 
[
	{
		name:		"design",
		back:		"/img/design 2.png",
		front:		"/img/design.png",
		category:	CATEGORIES.DESIGN
	},
	
	{
		name:		"front end",
		back:		"/img/frontend 2.png",
		front:		"/img/frontend.png",
		category:	CATEGORIES.FRONTEND
	},
	
	{
		name:		"back end",
		back:		"/img/backend 2.png",
		front:		"/img/backend.png",
		category:	CATEGORIES.BACKEND
	},
	
	{
		name:		"hardware",
		back:		"/img/hardware 2.png",
		front:		"/img/hardware.png",
		category:	CATEGORIES.HARDWARE
	},
	
	{
		name:		"writing",
		back:		"/img/writing 2.png",
		front:		"/img/writing.png",
		category:	CATEGORIES.WRITING
	}
];

import './iconer.less'

var Icon = props =>
	<div className="iconerClip" >
		<img src={ props.back } className="back"/>
		<div className="name">
			{ props.name }
		</div>
		<img src={ props.front } 
			 className="front clickable"
			 onClick={ props.onClick }/>
	</div>;
	
	
import './PortfolioCard.less';

var PortfolioCard = props =>
	<div className={"portfolioCard clickable" + " " + props.className}
		 onClick={ props.onClick }>
		<div className="cardImgSizer">
			<div className="cardImg" style={{ backgroundImage: `url("${props.image}")`}}/>
			<div className="cardImgBorder" />
		</div>
		<div className="cardTitle">
			{props.title}
		</div>
		<div className="cardSub1">
			{props.sub1}
		</div>
		<div className="cardSub2">
			{props.sub2}
		</div>
	</div>;
	
var cards =
{
	[CATEGORIES.DESIGN] : 
	[
		{
			image:		"/img/projects/coffee/coffee-card.png",
			title: 		"Coffee Vending Machine Android App",
			sub1:		"Illustrator, After Effects",
			sub2:		"Fall 2015, 2 months",
			className:	"transition reverse",
			
			project:	PROJECTS.COFFEE
		}
		
	],
	[CATEGORIES.FRONTEND] : 
	[
		{
			image:		"/img/projects/kaomoji/kaomoji-card.png",
			title: 		"Kaomoji Community Site App",
			sub1:		"React.js Redux, Silicon C++, MySQL",
			sub2:		"Fall 2016, 3 months",
			className:	"transition reverse",
			
			project:	PROJECTS.KAOMOJI
		}
	],
	[CATEGORIES.BACKEND]  : 
	[
		{
			image:		"/img/projects/kaomoji/kaomoji-card.png",
			title: 		"Kaomoji Community Site App",
			sub1:		"React.js Redux, Silicon C++, MySQL",
			sub2:		"Fall 2016, 3 months",
			className:	"transition reverse",
			
			project:	PROJECTS.KAOMOJI
		}
	],
	[CATEGORIES.HARDWARE] : 
	[
		{
			image:		"/img/projects/lamp/lamp-card.png",
			title: 		"Antibiotic Timed Blue Acne Lamp",
			sub1:		`Kiel ${u("00B5")}Vision4, STM32F4 Board, misc. hardware`,
			sub2:		"Fall 2015, 2 weeks",
			className:	"transition reverse",
			
			project:	PROJECTS.LAMP
		}	
	],
	[CATEGORIES.WRITING]  : 
	[
		{
			image:		"/img/projects/dopamine/dopamine-card.png",
			title: 		"Dopamine in the Mechanism of Action of SSRIs",
			sub1:		"Word",
			sub2:		"Spring 2015, 1 week",
			className:	"transition reverse",
			
			project:	PROJECTS.DOPAMINE
		}	
	]
}



const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

import './iconer.less'

var Icon2 = props =>
	<div className={`iconerClip2 ${props.current? " current" : ""}`}>
		<img src={ props.back } className="back"/>
		<div className="name">
			{ props.name }
		</div>
		<img src={ props.front } 
			 className="front clickable"
			 onClick={ props.onClick }/>
	</div>;
	
var Delayed = React.createClass
({
	getInitialState()
	{
		return (
		{ 
			loaded: false		
		});
	},
	
	componentDidMount()
	{
		window.setTimeout(() =>
		{
			this.setState({loaded: true});
		}, 300);
	},
	
	render()
	{
		return (
			<div className="cardsClip">
			{this.state.loaded?
				<div className="cards">
				{
					cards[this.props.selectedCategory] && 
					cards[this.props.selectedCategory].map((c, i) =>
						
						<PortfolioCard 
							image={c.image}
							title={c.title}
							sub1={c.sub1}
							sub2={c.sub2}
							className={c.className + ' delay' + i}
							
							key={c.title}
							
							onClick={ e=> 
								this.props.setProjectRoute(this.props.selectedCategory, c.project)
							}
						/>
					)
				}	
				</div>
			:null
			}
			</div>	
		)
	}
	
})
	

class PortfolioMobile extends React.Component
{
	constructor(props)
	{
		super(props)

		let index = props.selectedCategory? icons.findIndex((i) => props.selectedCategory === i.category) : 0;
		index = index === -1? 0 : index;
	
		this.state = 
		{
			currentCategory: icons[index].category,
			currentIndex: index,
			anyCategory: props.anyCategory, 
		};
		
		this.onClick 		= this.onClick.bind(this);
		this.onChangeIndex 	= this.onChangeIndex.bind(this);
		this.onSwitching 	= this.onSwitching.bind(this);
		
		this.componentWillReceiveProps 	= this.componentWillReceiveProps.bind(this);
		this.componentDidUpdate 		= this.componentDidUpdate.bind(this);

		this.render = this.render.bind(this);
		
		this.E = [];
	}
	
	// necessary if the user directly goes to a page
	componentWillReceiveProps(nextProps)
	{
		if(nextProps.selectedCategory !== this.props.selectedCategory)
		{
			if(nextProps.selectedCategory !== "")
				this.setState
				({
					currentCategory: nextProps.selectedCategory,
					currentIndex: icons.findIndex((i) => nextProps.selectedCategory === i.category)
				})
		}
		
		if(nextProps.anyCategory !== this.props.anyCategory)
		{

			if(this.state.anyCategory !== nextProps.anyCategory)
			{
				
				
				//none -> true (forward)
				if(nextProps.anyCategory)
				{
					window.setTimeout(() => this.setState({anyCategory: true})
					, 500);
					
					
					ReactDOM.findDOMNode(this.paginator).style.animationName = "fadeOut";
					
					let index = icons.findIndex((i) => nextProps.selectedCategory === i.category);
					var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
						
					let pad = (w - (w * 0.15)) / 4;		
					
					let prevSlide = this.E[index-1]? this.E[index-1].parentElement : null;
					let nextSlide = this.E[index+1]? this.E[index+1].parentElement : null;
					let slideHolder = ReactDOM.findDOMNode(this.root);
					
					
					let resetSlides = () => 
					{
						if(prevSlide)
						{
							prevSlide.style.transform = `translateX(0px)`;
							prevSlide.style.transition = "";
						}
						
						if(nextSlide)
						{
							nextSlide.style.transform = `translateX(0px)`;
							nextSlide.style.transition = "";
						}			
					}
					
					let forwardsSet = () =>
					{
						resetSlides();			
						slideHolder.style.padding = "0px";
					};
							
					let backwardsSet = ()=>
					{
						resetSlides();
						
						// rely on the CSS
						slideHolder.style.paddingLeft = "";
						slideHolder.style.paddingRight = "";
					};
						
					this.icons.style.transform = "translateY(-50%)";
					this.icons.style.transition = "transform .3s ease-in-out";
					
					backwardsSet();
					if(prevSlide)
					{
						prevSlide.style.transform = `translateX(-${pad}px)`;
						prevSlide.style.transition = `transform .3s ease-in-out`;
					}
					
					if(nextSlide)
					{
						nextSlide.style.transform = `translateX(${pad}px)`;
						nextSlide.style.transition = `transform .3s ease-in-out`;
					}
					
					let forwardsTimeout = window.setTimeout(forwardsSet, 301);	
				}
				
				// true -> false (backward)
				else
				{
					
					this.setState({anyCategory: false});
				}
				
			}
			
		}
		
		
	}
	
	componentDidUpdate(prevProps, prevState)
	{
		//console.log(this.props.selectedCategory === "");
		
		if(this.state.anyCategory !== prevState.anyCategory)
		{
		
			// backwards
			if(!this.state.anyCategory && prevState.anyCategory)
			{
				
				
				ReactDOM.findDOMNode(this.paginator).style.animationName = "";
				
				let index = icons.findIndex((i) => prevProps.selectedCategory === i.category);
				var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
					
				let pad = (w - (w * 0.15)) / 4;		
				
				let prevSlide = this.E[index-1]? this.E[index-1].parentElement : null;
				let nextSlide = this.E[index+1]? this.E[index+1].parentElement : null;
				let slideHolder = ReactDOM.findDOMNode(this.root);
				
				
				let resetSlides = () => 
				{
					if(prevSlide)
					{
						prevSlide.style.transform = `translateX(0px)`;
						prevSlide.style.transition = "";
					}
					
					if(nextSlide)
					{
						nextSlide.style.transform = `translateX(0px)`;
						nextSlide.style.transition = "";
					}			
				}
				
				let forwardsSet = () =>
				{
					resetSlides();			
					slideHolder.style.padding = "0px";
				};
						
				let backwardsSet = ()=>
				{
					resetSlides();
					
					// rely on the CSS
					slideHolder.style.paddingLeft = "";
					slideHolder.style.paddingRight = "";
				};
				
				forwardsSet();
				this.icons.style.transform = "translateY(-50%)";
				
				window.setTimeout(() => {
					this.icons.style.transform = "translateY(-56px)";
					this.icons.style.transition = "transform .3s ease-in-out";
					if(prevSlide)
					{
						prevSlide.style.transform = `translateX(${pad*2}px)`;
						prevSlide.style.transition = `transform .3s ease-in-out`;
					}
					
					if(nextSlide)
					{
						nextSlide.style.transform = `translateX(-${pad*2}px)`;
						nextSlide.style.transition = `transform .3s ease-in-out`;
					}
				
				}, 301);
				
				let backwardsTimeout = window.setTimeout(backwardsSet, 650);	
				
			}
		}
			
		
	}
	
	onClick(category, index)
	{
		let props = this.props;
		this.setState
		({
			currentCategory: icons[index].category,
			currentIndex: index,
		})			
	
		
		// moving backwards
		if(props.selectedCategory === category)
		{
			
			return props.setCategoryRoute("");
		}
		
		// moving forwards
		
		return props.setCategoryRoute(category);
	}
	
	onChangeIndex(index, indexLatest)
	{
		window.setTimeout( () => 
		this.setState
		({
			currentCategory: icons[index].category,
			currentIndex: index
		}), 10);	
	}
	
	onSwitching(index, type)
	{
		if(Number.isInteger(index))
		{
			this.setState
			({
				currentCategory: icons[Math.ceil(index)].category,
				currentIndex: index,
			})		

			window.setTimeout(()=>this.props.setCategoryRoute(icons[Math.ceil(index)].category), 300);
		}
	}
	
	render = () =>	
		<div className={`portfolioMobile ${this.state.anyCategory? "selected" : ""} ${this.props.anyProject? "projectSelected" : ""}`}>
			<div className="icons" ref={thisE => this.icons = thisE}>
				<AutoPlaySwipeableViews 
								autoplay={!this.state.anyCategory}
								interval={5000}							
								className="swipeable"
								ref={thisE => this.root = thisE}
								slideClassName="slide"
								onChangeIndex={this.onChangeIndex}
								onSwitching={this.state.anyCategory? this.onSwitching : () => {}}
								index={this.state.currentIndex}
								enableMouseEvents
								>
								
				{icons.map((i, index) => 
					<div className="slideContent" ref={thisE => this.E[index] = thisE}>
						<Icon2 name={i.name}
							   back={i.back}
							   front={i.front}
							   key={i.name}
							   
							   current={this.state.currentCategory === i.category}
							   
							   onClick={ e=> this.onClick(i.category, index)}
						/>     
					</div>
				)
				}
				</AutoPlaySwipeableViews>
			</div>
			{!this.state.anyCategory?
				<Paginator pages={5} 
						   currentIndex={this.state.currentIndex}
						   ref={thisE => this.paginator = thisE}/>
				: null
			}
			{this.state.anyCategory?
				<Delayed {...this.props}/>
				: null
			}
			
		</div>;
}

import './portfolio.less';

class Portfolio extends React.Component 
{
	constructor(props) 
	{
		super(props);
		// Operations usually carried out in componentWillMount go here
	}
	
	componentWillMount()
	{
		const props = this.props;
		
		props.setPagePortfolio();
		
		props.setCategory("");
		
		var splitLocation = [];		
		splitLocation = props.location.pathname.split('/');
		
		if(splitLocation && splitLocation.length > 0)
		{
			var page = splitLocation[1];
			var category = splitLocation[2];
			var project = splitLocation[3];
			
			if(page && page !== "portfolio")
			{
				props.setCategory("");
				props.setProject("");
				return;
			}
			
			props.setCategory("");
			if(category && category !== "")
			{
				props.setCategory(category);
			}
			
			props.setProject("");
			if(project && project !== "")
			{
				props.setProject(project);
			}

		}
	}
	
	componentDidMount()
	{
		if(this.props.location && this.props.location.hash !== "")
		{
			window.setTimeout(()=>
			{
				var anchor = this.props.location.hash.split("#")[1];
				var search = `a[name='${anchor}']`;
				if($(search)[0])
				{
					window.scrollTo(0, $(search).offset().top);
				}
			}
			, 1500);
		}
		
	}
	
	componentDidUpdate(prevProps)
	{
		const props = this.props;
			
		if(prevProps !== props)
		{
			var splitLocation = [];		
			splitLocation = props.location.pathname.split('/');
			
			if(splitLocation && splitLocation.length > 0)
			{
				var page = splitLocation[1];
				var category = splitLocation[2];
				var project = splitLocation[3];
				
				if(page && page !== "portfolio")
				{
					props.setCategory("");
					props.setProject("");
					return;
				}
				
				props.setCategory("");
				if(category && category !== "")
				{
					props.setCategory(category);
				}
				
				props.setProject("");
				if(project && project !== "")
				{
					props.setProject(project);
				}
			}
		}
	}
	
	render()
	{
		const props = this.props;
		
		//window.locationX = props.location;

		return(
		<div className="portfolioContainer">
			<PageTitle title={props.anyProject? props.projectTitle : props.anyCategory? NAMES[props.selectedCategory] : "Portfolio"}/>
		<MediaQuery query='(min-width: 601px)'>
		{
			<div className={"fullHeight page portfolio skipTop" + 
						    (props.anyCategory? " selected" :"")}>
				<div className="holdsIcons">
				{
					icons.map(i => 
						<div className={"iconer" + (props.selectedCategory === i.category? " selected" : "")}>
							<Icon name={i.name}
								  back={i.back}
								  front={i.front}
								  key={i.name}
								  
								  onClick={ e=>
								  {
									if(props.selectedCategory === i.category)
										return props.setCategoryRoute("");
									return props.setCategoryRoute(i.category);
								  }}
							/>
						</div>
					)
				}
				</div>
				
				<div className="holdsContentClip">
					<div className="holdsContent">
					{
						cards[props.selectedCategory] && 
						cards[props.selectedCategory].map((c, i) =>
							
							<PortfolioCard 
								image={c.image}
								title={c.title}
								sub1={c.sub1}
								sub2={c.sub2}
								className={c.className + ' delay' + i}
								
								key={c.title}
								
								onClick={ e=> 
									props.setProjectRoute(props.selectedCategory, c.project)
								}
							/>
						)
					}						  
					</div>
				</div>	
			</div>
		}
		
		</MediaQuery>
		<MediaQuery query='(max-width: 600px)'>
			<PortfolioMobile {...props}/>
		</MediaQuery>

		
		<div className={"projectClip" + (props.anyProject? " selected" : "")}>
			{props.anyProject?
				<Project/>
			:
				null
			}
		</div>
		

		</div>
		);
	}
};
Portfolio = RRedux.connect(
	
	(state, ownProps) => 
	({
		anyCategory:		state.Portfolio.category !== '',
		selectedCategory:	state.Portfolio.category,
		
		anyProject:			state.Project.project !== "",
		projectTitle:		state.Project.title,
		
		location:			ownProps.location
	}),
	
	d => 
	({	
		setCategoryRoute(category) 	{ d(changeCategoryRoute(category)) },
		setCategory(category) 		{ d(changeCategory(category)) },
		setPagePortfolio() 			{ d(changePage(PAGES.PORTFOLIO)) },
		
		setProjectRoute(category, project) 	{ d(setProjectRoute(category, project)) },
		setProject(project) 				{ d(setProject(project)) }
	})
	
)(Portfolio);

		


	
export default Portfolio;