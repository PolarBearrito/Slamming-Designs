const React = require('react');

var Waypoint = require('react-waypoint');

export var P = React.createClass
({
	getInitialState()
	{
		return (
		{ 
			loaded: false
		});
	},
	
	render()
	{
		return(
			<Waypoint bottomOffset="100px" onEnter={() => {this.setState({loaded: true})}}
			>
				<p style={{opacity: this.state.loaded? "1" : "0"}}>
				{this.props.children}
				</p>
			</Waypoint>
		)
	}
});

export var UL = React.createClass
({
	getInitialState()
	{
		return (
		{ 
			loaded: false
		});
	},
	
	render()
	{
		return(
			<Waypoint bottomOffset="100px" 
					  onEnter={() => {this.setState({loaded: true})}}
			>
				<ul style={{opacity: this.state.loaded? "1" : "0"}}>
				{this.props.children}
				</ul>
			</Waypoint>
		)
	}
});