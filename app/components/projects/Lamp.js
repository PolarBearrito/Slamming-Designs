const React = require('react');
const RRedux = require('react-redux');

import Content from './Content';
import {
	SubHeading,
	Heading
} from './SubHeading';
import ExpandableImage from "./ExpandableImage";



export var Lamp = props =>
	<Content>
	
		<Heading title="The Basics" name="the-basics"/>
		<SubHeading title="working on it..." name="working-on-it"/>
		<p>
			I'm working on the write-up. I will be uploading the code soon.
		</p>
		<p>
			However, the included <a href="#fast-facts-deliverables">link</a> summarizes pretty much everything about the concept, from it's theoretical backings to the operation and code. The document contains code, state machine diagrams, circuit diagrams, references to medical studies, etc. 
			<br/>
			It was the document I submitted as a part of my submission for class, it was written up relatively quick, but is 40 pages of explanation with an addition 20 pages of code.
		</p>
		
		<p>
			The write-up I will provide here will be an expansion on my motivations/research and what I did and will mostly summarize the linked document as I know it is very long.
		</p>
		
		<div className="emptySpace">
			&nbsp;
		</div>
		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>
	</Content>;
	
export default Lamp;