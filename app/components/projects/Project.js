import React from 'react';
const RRedux = require('react-redux');

import { PROJECTS_NARRATIVES } from '../../redux/reducers/Projects';
import { changeCategoryRoute } from '../../redux/reducers/PortfolioReducer';

import { browserHistory } from 'react-router';

import {
	Navigator,
	NavigatorFloating
} from './Navigator';
import {
	FastFacts,
	FastFactsResponsive,
	} from './FastFacts';
	
var MediaQuery = require('react-responsive');
import ReactDOM from 'react-dom';

var Waypoint = require('react-waypoint');

var DelayedImage = React.createClass
({
	getInitialState()
	{
		return (
		{ 
			loaded: false		
		});
	},
	
	componentWillMount()
	{
		window.setTimeout(() =>
		{
			this.setState({loaded: true});
		}, 500);
	},
	
	render()
	{
		return(this.state.loaded? <img src={this.props.src}/> : null)
	}
	
})


class Project extends React.Component
{
	constructor(props)
	{
		super(props)
		
		this.state = 
		{
			transitioned: false,
			currentFFHeight: 0,
			currentNavHeight: 0
		}
	
		this.ffOnChangeHeight = this.ffOnChangeHeight.bind(this);
		this.navOnChangeHeight = this.navOnChangeHeight.bind(this);

		this.render = this.render.bind(this, this);
	}
		
	
	ffOnChangeHeight(newHeight, oldHeight)
	{
		if(!this.nav)
			return;
		
		if(!this.narrative)
			return;
		
		if(this.timeout)
		{			
			window.clearTimeout(this.timeout);
			this.timeout = null;
		}
		
		this.nav = ReactDOM.findDOMNode(this.nav);
		
		
		let nextTrans = function()
		{
			this.nav = ReactDOM.findDOMNode(this.nav);
			
			this.nav.style.transform = `translateY(${newHeight - this.state.currentFFHeight}px)`;
			this.nav.style.transition = "transform 0.35s cubic-bezier(0.15, 0.3, 0.25, 1)";
			
			this.narrative.style.transform = `translateY(${newHeight - this.state.currentFFHeight}px)`;
			this.narrative.style.transition = "transform 0.35s cubic-bezier(0.15, 0.3, 0.25, 1)";
		}
		nextTrans = nextTrans.bind(this);
		
		if(!this.state.transitioned)
		{
			this.nav.style.transform = `translateY(${oldHeight - this.state.currentFFHeight}px)`;
			this.nav.style.transition = "";
			
			this.narrative.style.transform = `translateY(${oldHeight - this.state.currentFFHeight}px)`;
			this.narrative.style.transition = "";
			
			this.setState({transitioned: true});
			
			window.setTimeout(nextTrans, 10);		
		}
		
		else
		{
			nextTrans();
		}
		
		let navHeight = $(ReactDOM.findDOMNode(this.nav)).outerHeight();	
		
		let fn = function()
		{
			this.nav = ReactDOM.findDOMNode(this.nav);
			
			this.nav.style.transition = "";
			this.nav.style.marginTop = `${newHeight}px`;
			this.nav.style.transform = "";	
			
			this.setState
			({ 
				currentFFHeight: newHeight,
			});

			this.timeout = null;
			
			
			this.timeout2 = window.setTimeout(fn2, 1);
		};		
		fn = fn.bind(this);		
		
		this.timeout = window.setTimeout(fn, 1050);
		
				
		if(this.timeout2)
			window.clearTimeout(this.timeout2);
		
		let fn2 = function()
		{
			this.narrative.style.transition = "";
			this.narrative.style.marginTop = `${this.state.currentFFHeight+ this.state.currentNavHeight}px`;
			this.narrative.style.transform = "";
		};
		fn2 = fn2.bind(this);
	
		
	}
	
	navOnChangeHeight(newHeight)
	{
		if(!this.narrative)
			return;
		
		this.narrative.style.transform = `translateY(${newHeight - this.state.currentNavHeight}px)`;
		this.narrative.style.transition = "transform 0.35s cubic-bezier(0.15, 0.3, 0.25, 1)";
				
		if(this.timeout2)
		{			
			window.clearTimeout(this.timeout2);
			this.timeout2 = null;
		}

		let fn2 = function()
		{
			this.narrative.style.transition = "";
			this.narrative.style.marginTop = `${this.state.currentFFHeight+ newHeight}px`;
			this.narrative.style.transform = "";
			
			this.setState
			({ 
				currentNavHeight: newHeight
			});
		};
		fn2 = fn2.bind(this);
		
		this.timeout2 = window.setTimeout(fn2, 1050);
			
		
	}
	
	render = ({props}) =>
		<div className="fullWidth project">
		
			<div className="heroImage" style={ {backgroundColor: props.color} }>
				<div className="boxShadow"/>
				<div className="page">
					<DelayedImage src={props.image}/>
					<i className="icon-arrow-left2 backArrow" aria-hidden="true" 
					   onClick={props.resetProjectRoute}/>
				</div>
			</div>
			
			<div className="content">

				<div className="leftBack"/>
				
				<div className="page">
			
					<div className="title">
					<MediaQuery query='(min-width: 1101px)'>
						<div className="left"/>
					</MediaQuery>
						<div className="center">
							<div className="titleText">
								{props.title}
							</div>
							<div className="description">
								{props.description}
							</div>
							<h4><i className="fa fa-calendar" aria-hidden="true"/>&nbsp;
								{props.when}
							</h4>
							<h4><i className="fa fa-clock-o" aria-hidden="true"/>&nbsp;
								{props.duration}
							</h4>
						</div>
						
					</div>
				</div>
			<MediaQuery query='(max-width: 600px)'>
				<FastFactsResponsive ref={thisElm => this.ff = thisElm}
									 onChangeHeight={this.ffOnChangeHeight}/>
			</MediaQuery>
			
			<MediaQuery query='(max-width: 600px)'>
				<NavigatorFloating ref={thisElm => this.nav = thisElm} onChangeHeight={this.navOnChangeHeight}/>
			</MediaQuery>
			
			<MediaQuery query='(min-width: 601px)'>
				<FastFactsResponsive ref={thisElm => this.ff = thisElm}/>
			</MediaQuery>
			
				<div className="page" ref={thisElm => this.narrative = thisElm}>
				
					<div className="narrative">
					<MediaQuery query='(min-width: 1101px)'>
						<div className="left">
							<Navigator/>
						</div>
					</MediaQuery>
						<div className="center">
							{PROJECTS_NARRATIVES[props.project] && PROJECTS_NARRATIVES[props.project]()}
						</div>
						
					</div>
				
				</div>
			</div>
		
		</div>;
}
Project = RRedux.connect(
	
	s => 
	({
		project: s.Project.project,
		
		color: s.Project.color,
		title: s.Project.title,
		image: s.Project.image,
		
		description: s.Project.description,
		
		when: 			s.Project.when,
		duration: 		s.Project.duration,
		
		currentCategory: s.Portfolio.category 
	}),
	
	null,
	
	(sp, {dispatch: d}) => Object.assign({}, sp, 
	{
		resetProjectRoute() {d(changeCategoryRoute(sp.currentCategory))}		
	})
	
)(Project);



	
export default Project;