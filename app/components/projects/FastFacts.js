import React from 'react';
const RRedux = require('react-redux');

var ReactGA = require('react-ga');

import { PROJECTS_NARRATIVES } from '../../redux/reducers/Projects';

import Navigator from './Navigator';
import DisplayIf from './DisplayIf';

import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

var MediaQuery = require('react-responsive');

import { Paginator2 } from '../../helpers/Paginator';

import ReactDOM from 'react-dom';

/*		
		when: 			"",
		duration: 		"",
		ingredients: 	[],
		individual: 	true,
		others: 		[],
		class: 			"",
		tags: 			[],
		deliverables: 	[]
*/

class Section extends React.Component
{
	constructor(props)
	{
		super(props);
		
		this.render = this.render.bind(this);
	}
	
	
	render()
	{
		return(
			<div className={`section ${this.props.centered? "centered" : ""}`}>
			{!this.props.centered?
				<img className="sectionImg" src={this.props.src}/>
			:null}
				<div className="content">
				{this.props.centered?
					<img className="sectionImg" src={this.props.src}/>
				:null}
					<h3>
						{this.props.title}
						<a name={this.props.name}>&nbsp;</a>
					</h3>
					{this.props.children}
				</div>
			</div>
		)		
	}
	
	
	
	
}

var Deliverables = (props) =>
	<Section centered={props.centered}
			 src="/img/projects/fastfacts/deliverables.png"
			 title="deliverables"
			 name="fast-facts-deliverables"
			 >
		<ul className="deliverables">
		{
			props.deliverables.map
			(
				(deliverable, idx) =>
				<li key={idx}>
					<ReactGA.OutboundLink
						eventLabel={deliverable.label}
						target="_self"
						to={deliverable.link}>
						{deliverable.text}
					</ReactGA.OutboundLink>
				</li>
			)
		}
		</ul>
	</Section>;
Deliverables = RRedux.connect(
	
	s => 
	({
		deliverables: 	s.Project.deliverables,		
	}),
	
)(Deliverables);

var Roles = (props) =>
	<Section centered={props.centered}
			 src="/img/projects/fastfacts/roles.png"
			 title="roles"
			 >
		<ul className="roles">
		{
			props.roles.map
			(
				(role, idx) => <li key={idx}>{role}</li>
			)
		}
		</ul>
	</Section>;
Roles = RRedux.connect(
	
	s => 
	({
		roles: 	s.Project.roles,		
	}),
	
)(Roles);

var Ingredients = props =>
	<Section centered={props.centered}
			 src="/img/projects/fastfacts/ingredients.png"
			 title="ingredients"
			 >
		<ul className="ingredients">
		{
			props.ingredients.map
			(
				(ingredient, idx) => <li key={idx}>{ingredient}</li>
			)
		}
		</ul>
	</Section>;
Ingredients = RRedux.connect(
	
	s => 
	({
		ingredients: 	s.Project.ingredients,		
	}),
	
)(Ingredients);

var Tags = props =>
	<Section centered={props.centered}
			 src="/img/projects/fastfacts/tags.png"
			 title="tags"
			 >
		<ul className="tags">
		{
			props.tags.map
			(
				(tag, idx) => <li key={idx}>{tag}</li>
			)
		}
		</ul>
	</Section>;
Tags = RRedux.connect(
	
	s => 
	({
		tags: 	s.Project.tags,		
	}),
	
)(Tags);

var Class = props =>
	<Section centered={props.centered}
			 src="/img/projects/fastfacts/class.png"
			 title="class"
			 >
		<p>{props.class}</p>
	</Section>;
Class = RRedux.connect(
	
	s => 
	({
		class: 	s.Project.class,		
	}),
	
)(Class);

var Others = props =>
	<Section centered={props.centered}
			 src="/img/projects/fastfacts/members.png"
			 title="other members"
			 >
		<ul className="members">
		{
			props.others.map
			(
				(member, idx) => <li key={idx}>{member}</li>
			)
		}
		</ul>
	</Section>;
Others = RRedux.connect(
	
	s => 
	({
		others: 	s.Project.others,		
	}),
	
)(Others);

	

import './FastFacts.less';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

export class FastFactsResponsive extends React.Component
{
	constructor(props)
	{
		super(props);
		
		this.state = 
		{
			count: 	 0 	+ (this.props.displayDeliverables? 1 : 0)
						+ (this.props.displayTags? 1 : 0)
						+ (this.props.displayIngredients? 1 : 0)
						+ (this.props.displayRoles? 1 : 0)
						+ (this.props.displayClass? 1 : 0)
						+ (this.props.displayOthers? 1 : 0),
			currentIndex: 0,
			baseHeight: 0,
			height: 0,
			transitioned: false
		}
		
		this.slides = [];
		
		this.componentDidMount 	= this.componentDidMount.bind(this);
		
		this.onChangeIndex 	= this.onChangeIndex.bind(this);
		this.getChildren 	= this.getChildren.bind(this);
		this.componentWillReceiveProps 	= this.componentWillReceiveProps.bind(this);
		
		this.render = this.render.bind(this);
		
	}

	componentDidMount()
	{
		let ffHeight = $(this.ff).outerHeight();
		
		this.setState
		({
			height: ffHeight			
		});
		
		if(this.props.onChangeHeight)
			this.props.onChangeHeight(ffHeight);
	}
	
	onChangeIndex(index)
	{
		this.getStyle = () => {};
		
		this.ff.style.position = "absolute";
		
		let slideHeight = $(ReactDOM.findDOMNode(this.slides[index])).outerHeight();
		
		if(!this.state.transitioned)
		{
			let oldHeight = $(ReactDOM.findDOMNode(this.slides[this.state.currentIndex])).outerHeight();
			
			if(this.props.onChangeHeight)
			{
				this.props.onChangeHeight(this.state.baseHeight + slideHeight, this.state.baseHeight + oldHeight);
			}
		}
		
		else
		{
			if(this.props.onChangeHeight)
			{
				this.props.onChangeHeight(this.state.baseHeight + slideHeight);
			}
			
		}
		
		this.setState
		({
			currentIndex: index,
			height: this.state.baseHeight + slideHeight
		});
		

		
	}
	
	componentWillReceiveProps(nextProps)
	{
		if(nextProps !== this.props)
		{
			this.setState({			
			count: 	 0 	+ (nextProps.displayDeliverables? 1 : 0)
						+ (nextProps.displayTags? 1 : 0)
						+ (nextProps.displayIngredients? 1 : 0)
						+ (nextProps.displayRoles? 1 : 0)
						+ (nextProps.displayClass? 1 : 0)
						+ (nextProps.displayOthers? 1 : 0)
			});
		}
		
	}
	
	getChildren(centered)
	{
		let props = this.props;
		let children = [];
		let index = 0;
		if(props.displayDeliverables)
		{
			let fn = index => thisElm => this.slides[index] = thisElm;
			
			children.push(<Deliverables centered={centered} ref={fn(index)} />);
			index++;
		}
		if(props.displayRoles)
		{
			let fn = index => thisElm => this.slides[index] = thisElm;
			
			children.push(<Roles centered={centered} ref={fn(index)}/>);
			index++;
		}
		if(props.displayIngredients)
		{
			let fn = index => thisElm => this.slides[index] = thisElm;
			
			children.push(<Ingredients centered={centered} ref={fn(index)}/>);
			index++;
		}
		if(props.displayTags)
		{
			let fn = index => thisElm => this.slides[index] = thisElm;
			
			children.push(<Tags centered={centered} ref={fn(index)}/>);
			index++;
		}
		if(props.displayClass)
		{
			let fn = index => thisElm => this.slides[index] = thisElm;
			
			children.push(<Class centered={centered} ref={fn(index)}/>);
			index++;
		}
		if(props.displayOthers)
		{
			let fn = index => thisElm => this.slides[index] = thisElm;
			
			children.push(<Others centered={centered} ref={fn(index)}/>);
			index++;
		}
		
		return children;
		
	}
	
	render = () =>
		<div className="fastFactsResponsive" ref={thisElm => this.ff = thisElm}>
			<div className="title">
				<img src="/img/projects/fastfacts/fast facts.png"/>
				<h2> Fast Facts </h2>
				
			<MediaQuery query='(max-width: 600px)'>
				<Paginator2 
							pages={this.state.count} 
							currentIndex={this.state.currentIndex}
							onClick={this.onChangeIndex}/>
			</MediaQuery>
			
			</div>
			
			<MediaQuery query='(min-width: 601px)'>
			<div className="content">
				<div className="page">
				{this.getChildren(false)}
				</div>
			</div>
			</MediaQuery>
			

			<MediaQuery query='(max-width: 600px)'>
			<div className="content">
				<SwipeableViews 
								animateHeight						
								className="swipeable"
								ref={thisE => this.root = thisE}
								slideClassName="slide"
								onChangeIndex={this.onChangeIndex}
								index={this.state.currentIndex}
								enableMouseEvents
								>
								{this.getChildren(true)}
				</SwipeableViews>
			</div>
			</MediaQuery>
			
			<div className="fastFactsFooter">

			</div>
		</div>
	
	
}
FastFactsResponsive = RRedux.connect(
	
	s => 
	({
		ingredients: 	s.Project.ingredients,
		individual: 	s.Project.individual,
		others: 		s.Project.others,
		roles: 			s.Project.roles,
		class: 			s.Project.class,
		
		tags: 			s.Project.tags,
		deliverables: 	s.Project.deliverables,
		
		displayDeliverable: s.Project.deliverables && 
							s.Project.deliverables.length > 0,
							
		displayTags: 		s.Project.tags && 
							s.Project.tags.length > 0,
							
		displayIngredients: s.Project.ingredients && 
							s.Project.ingredients.length > 0,
							
		displayRoles: 		s.Project.roles && 
							s.Project.roles.length > 0,
		displayClass:		!!s.Project.class,
							
		displayOthers: 		!s.Project.individual && 
							s.Project.others && 
							s.Project.others.length > 0,
							
		displayDeliverables: 	s.Project.deliverables && 
								s.Project.deliverables.length > 0,
		
	}),
	
	d =>
	({

	})
	
)(FastFactsResponsive);



export var FastFacts = props =>
	<div className="fastFacts">
	
		<DisplayIf className="section" if={props.deliverables && props.deliverables.length > 0}>
			<h3>deliverables<a name="fast-facts-deliverables">&nbsp;</a></h3>
			<ul className="deliverables">
			{
				props.deliverables.map
				(
					(deliverable, idx) =>
					<li key={idx}>
						<ReactGA.OutboundLink
							eventLabel={deliverable.label}
							target="_self"
							to={deliverable.link}>
							{deliverable.text}
						</ReactGA.OutboundLink>
					</li>
				)
			}
			</ul>
		</DisplayIf>
		
		<DisplayIf className="section" if={props.tags && props.tags.length > 0}>
			<h3>tags</h3>
			<ul className="tags">
			{
				props.tags.map
				(
					(tag, idx) => <li key={idx}>{tag}</li>
				)
			}
			</ul>
		</DisplayIf>
		
		<DisplayIf className="section" if={props.ingredients && props.ingredients.length > 0}>
			<h3>ingredients</h3>
			<ul className="ingredients">
			{
				props.ingredients.map
				(
					(ingredient, idx) => <li key={idx}>{ingredient}</li>
				)
			}
			</ul>
		</DisplayIf>
	
		<DisplayIf className="section" if={props.individual}>
			<h4>{props.individual? "individual project" : "group project"}</h4>
		</DisplayIf>
		
		<DisplayIf className="section" if={props.class}>
			<h3>class</h3>
			<p>{props.class}</p>
		</DisplayIf>
		
		<DisplayIf className="section" if={props.roles && props.roles.length > 0}>
			<h3>my roles</h3>
			<ul className="roles">
			{
				props.roles.map
				(
					(role, idx) => <li key={idx}>{role}</li>
				)
			}
			</ul>
		</DisplayIf>	
		
		<DisplayIf className="section" if={!props.individual && props.others}>
			<h3>other members</h3>
			<ul className="members">
			{
				props.others.map
				(
					(member, idx) => <li key={idx}>{member}</li>
				)
			}
			</ul>
		</DisplayIf>

	</div>;
FastFacts = RRedux.connect(
	
	s => 
	({
		ingredients: 	s.Project.ingredients,
		individual: 	s.Project.individual,
		others: 		s.Project.others,
		roles: 			s.Project.roles,
		class: 			s.Project.class,
		
		tags: 			s.Project.tags,
		deliverables: 	s.Project.deliverables
	}),
	
	d =>
	({

	})
	
)(FastFacts);



	
export default FastFacts;