import React from 'react';

// Summary: Uniform wrapper element for selectively displaying elements.

var DisplayIf = React.createClass
({
    render()
    {
        if (this.props.if)
        {
            if (React.Children.count(this.props.children) > 1)
                return (<div className={this.props.className}>{this.props.children}</div>);

            return (React.Children.only(this.props.children));
        }

        if (this.props.replacement)
        {
            if(typeof this.props.replacement ===  "string")
                switch(this.props.replacement) 
                {
                    case 'div':     return (<div/>);
                    case 'empty':   return (null);

                    default:        return (null);
                }
            
            if(React.Children.count(this.props.replacement) > 1)
                return (<div>{this.props.replacement}</div>);

            return (this.props.replacement);
        }

        return (null);
    }
});

export default DisplayIf;