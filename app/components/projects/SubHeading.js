import React from 'react';
const RRedux = require('react-redux');
var Waypoint = require('react-waypoint');

import { setProjectCurrHeaderIdx, decreaseHeader } from '../../redux/reducers/ProjectReducer';

export var Heading = props =>
	<Waypoint bottomOffset="200px" onEnter={props.setHeader.bind(null, props.index)}
								   onLeave=
								   {
										({currentPosition}) => 
										{
											if(currentPosition === Waypoint.below)
												props.decreaseHeader()
										}
								   }
	>
		<h1 className="hoverAnchor">
			<a name={props.name} href={`#${props.name}`}>
				<span>
					{props.title}
				</span>
			</a>
		</h1>
	</Waypoint>;
Heading= RRedux.connect(
	
	s => ({	}),
	
	d =>
	({
		setHeader(index) { d(setProjectCurrHeaderIdx(index)); },
		decreaseHeader() { d(decreaseHeader()); }
	})
	
)(Heading);
Heading.displayName = "heading";

export var SubHeading = props =>
	<Waypoint bottomOffset="200px" onEnter={props.setHeader.bind(null, props.index)}
								   onLeave=
								   {
										({currentPosition}) => 
										{
											if(currentPosition === Waypoint.below)
												props.decreaseHeader()
										}
								   }
	>
		<h2 className="hoverAnchor">
			<a name={props.name} href={`#${props.name}`}>
				<span>
					{props.title}
				</span>
			</a>
		</h2>
	</Waypoint>;
SubHeading= RRedux.connect(
	
	s => ({	}),
	
	d =>
	({
		setHeader(index) { d(setProjectCurrHeaderIdx(index)); },
		decreaseHeader() { d(decreaseHeader()); }
	})
	
)(SubHeading);
SubHeading.displayName = "subheading";
	
export default SubHeading;