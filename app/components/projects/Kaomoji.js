const React = require('react');
const RRedux = require('react-redux');

import Content from './Content';
import DisplayIf from './DisplayIf';
import {
	SubHeading,
	Heading
} from './SubHeading';
import ExpandableImage from "./ExpandableImage";

import {P, UL} from "./ContentComponents";

export var Kaomoji = props =>
	<Content>
	
		<Heading title="The Basics" name="the-basics"/>
		<SubHeading title="working on it..." name="working-on-it"/>
		<p>
			I'm working on the write-up.
		</p>
		<p>
			<a href="#fast-facts-deliverables">Parts of the project are available</a>, but the complexity of this project lies in designing the architecture which the write-up will explain in depth.
		</p>
		
		<SubHeading title="what was this about?" name="what-was-this-about"/>
		<P>
			This was a capstone project, specifically, for Computer Science majors (although I chose to do this project as a Computer Engineer). As a capstone project it was very <b>involved</b>. The project revolved around producing a software product. You could choose a pre-defined project or create your own. Most students chose the latter. The requirements for custom projects weren't well-defined but generally were:
		</P>
		<UL>
			<li>
				Work in teams of 3-4
			</li>
			<li>
				Must involve a database
			</li>
			<li>
				Must involve communication between portions of the application
			</li>
			<li>
				Must perform at least 4 functions
			</li>
		</UL>
		<P>
			Realizing, the project, however, was much more comprehensive, spanning across multiple disciplines. <em>This was a class on software development <b>management</b>.</em> We weren't just learning how to become developers, we already knew how, NYU was teaching us to become <b>leads and managers</b>. Overall the project involved:
		</P>
		<UL>
			<li>Product Design</li>
			<li>Risk Assessment</li>
			<li>Project Management</li>
			<li>Software Architecture</li>
			<li>Software Project Documentation</li>
		</UL>
		<P>
			Essentially we modeled software development in a professional enterprise setting, creating presentations (<a href="#fast-facts-deliverables">the links have the presentation</a>), and writing IEEE standard documents. We had to code/architect/plan a project (<a href="#fast-facts-deliverables">check links for a demo</a>).
		</P>
		<P>
			The complexity of the capstone project ended up not being with the code itself, but rather, it lies with the architecture, planning, and management (this will be expanded on in this write-up).
		</P>
		
		<SubHeading title="how did we do?" name="how-did-we-do"/>
		<P>
			We received an A. The project wasn't fully completed due to heavy documentation and time constraints, however, it was never meant to be fully completed.
		</P>
		<P>
			Instead, I managed to set up the development environment to allow for full development.
		</P>
		
		<SubHeading title="what did I do?" name="what-did-I-do"/>
		<P>
			Notice that in the previous section I said "I", not "we", that is because, due to time constraints, my experience, and general team member (student) apathy, I pretty much did everything.
		</P>
		<P>
			Let me elaborate a bit more. It was a culmination of multiple factors:
		</P>
		<UL>
			<li>
				I was the most skilled, experienced, knowledgeable, and prepared
			</li>
			<li>
				I had already been professionally developing software for more than a year
			</li>
			<li>
				I had known about this class beforehand so I was prepared with sample documents
			</li>
			<li>
				The project idea we pursued was mine, everything was in my head
			</li>
			<li>
				My team members let me freely take the helm
			</li>
			<li>
				I had the most stake in this project, I personally wanted to eventually launch it, so I cared the most.
			</li>
			<li>
				We didn't have enough time to work on fully developing the project
			</li>
		</UL>
		
		
		
		
		
		
		
		<hr/>
		<Heading title="The Product" name="the-basics"/>
		<SubHeading title="product design" name="product-design"/>
		<P>
			So, what exactly was the product?
		</P>
		<P>
			Kaomoji. Kaomoji originated from the same place emoji originated from, Japan. Essentially, kaomojis are right side up emoticons, so rather than :), we have the ubiquitous shrug: ¯\_(ツ)_/¯.
		</P>
		<P>
			Emojis were quickly gaining traction, in many text oriented environments, but in some environments, kaomoji dominated. One such example might be reddit, the universal forum site, where kaomoji and other text-based pictograms can be seen being used. The idea was to capitalize on its growing usage.
		</P>
		<P>
		
		
		</P>
		<div className="emptySpace">
			&nbsp;
		</div>
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>
	</Content>;
	
export default Kaomoji;