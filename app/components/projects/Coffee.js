const React = require('react');
const RRedux = require('react-redux');

import Content from './Content';
import {
	SubHeading,
	Heading
} from './SubHeading';
import ExpandableImage from "./ExpandableImage";

var Waypoint = require('react-waypoint');

import {P, UL} from "./ContentComponents";


export var Coffee = props =>
	<Content>
		<Heading title="The Basics" name="the-basics"/>
		<SubHeading title="what was this about?" name="what-was-this-about"/>
		<P>
			This was a final project for, <b>Human-Computer Interface</b>. The skills that were utilized in this project included product design and UI/UX design. It represents a portion of my design skillset.
		</P>
		<P>
			The basic requirements were:		
		</P>
		<UL>
			<li>In teams of 4 or more students</li>
			<li>"Design a kiosk/technology for outdoor use by a specific group of people to perform some task(s)"</li>
			<li>"Project will involve designing a number of aspects of the kiosk based on usability and HCI theories throughout the term in and out of class"
			</li>
			<li>"Goal is to have a prototype, modelled virtually or physically, by the end of the term"</li>
		</UL>
		<SubHeading title="how did we do?" name="how-did-we-do"/>
		<P>
			Really well! 
		</P>
		<UL>
			<li className="listA">		
			We got an A
			</li>
			<li className="listHands">
			Other teams specifically praised our presentation
			</li>
			<li className="listFund">
			Our professor believed our project had funding potential
			</li>
		</UL>
		<P>
			The final presentation is linked in the <a href="#fast-facts-deliverables">links section</a> if you'd like to check it out.
		</P>
		<SubHeading title="what did I do?" name="what-did-i-do"/>
		<P>
			At the time I had already been working as a UI/UX designer in a professional environment. I was designing interfaces that would be used by a few hundred facility workers and managers. Design-wise, I was the most skilled and experienced so naturally, I took lead and I took the role of UI/UX designer. There were no objections, I mean, I <i>did volunteer</i> to do most of the work.
		</P>
		<P>
			I ended up doing all of the prototyping work--the mockups and the renderings. Everyone else helped to think of ideas, everyone worked on the user and task analyses, and everyone submitted written work.
		</P>
		<P>
			In fact, the ideas the team came up with were a strong influence on the overall direction of the project. What I <i>really did</i> was refine, realize, and make practical the ideas the team came up with.
		</P>
		<P>
			That's actually going to be a theme. Throughout the project, I would take our team's ideas and I would run with it. I'd flesh it out and refine it using my own experience and knowledge.
		</P>
		
		
		

		<hr/>
		<Heading title="The Idea" name="the-idea"/>
		<SubHeading title="humble beginnings" name="humble-beginnings"/>
		<P>
			Our team threw ideas until one of us randomly decided
			<q>
				let's just do a coffee vending machine...
			</q>
			No one was opposed. It was going to specifically vend coffee. The idea remained mostly enigmatic until we had to start designing the machine. 
		</P>
		<ExpandableImage src="/img/projects/coffee/coffee sketch 1.png" 
						 largeSrc="/img/projects/coffee/coffee sketch 1 large.png"
						 caption="A wild early sketch!"/>		
		<SubHeading title="a simple constraint drives everything" name="simple-constraint"/>
		<P>
			Dan insisted on minimalism:
			<q>
				I should be able to push a button and get my drink
			</q>
			To him, the less interface, the less interaction, the better. He wanted the process to be simple and straightforward. I wasn't on board at first, I mean, <i>how would we have material to meet the HCI requirements?</i>, but this constraint would drive the defining philosophy of our product.
		</P>
		

		
		
		<hr/>
		
		<Heading title="The Design" name="the-design"/>
		<SubHeading title="physical design" name="physical-design"/>
		<P>
			The team spent a day discussing the physical design, I made a sketch. <br/>
			Primarily tubular structure, a port for the cup, Wi-Fi to download the app. 
		</P>
		<ExpandableImage src="/img/projects/coffee/coffee sketch 2.png" caption="Another wild early sketch!"/>

		<SubHeading title="the virtues of fun" name="virtues-of-fun"/>
		<P>
			As I studied material design and I watched the trends in design, I noted that one big trending feature was entertainment. 
		</P>
		<P>
			Some of the bells and whistles like radial feedback in the trends such as <a href="https://material.io/guidelines/motion/choreography.html#">meaningful transitions</a> weren't there for any particular use case other than to make the experience enjoyable. <b>And that concept leaked into my own philosophy on design.</b> Between 2 similarly functioning, similarly usable apps, the design might be the defining differentiation between the 2.
		</P>
		<P>
			In my professional work, the app we were developing was commissioned mostly by the higher-ups to help them manage the process of assigning, tracking, and managing work. It wasn't being petitioned for by the facility workers themselves, in a way it was something coming from the bureaucracy designed to reduce bloat. 
		</P>
		<P>
			I knew that to help improve user acceptance, the app must make things easier for the workers, but also it must make the experience fun. 
		</P>
		<P>			
			<em>That extra touch of entertainment would help to get them on board.</em> 
		</P>
		
		<SubHeading title="touches of fun" name="touches-of-fun"/>
		<P>
			So how did I do it?
		</P>
		<P>
		In following my philosophy, I pushed for a window to show the drink being made as the cup traveled around the machine and an led screen exterior. 
		</P>
		<P>
			The screen was a contentious idea, but I justified it on the following:
		</P>
		<UL>
			<li>Marketing of new/seasonal offers</li>
			<li>Advertising the app</li>
			<li>Third party advertising</li>
			<li>Fun, the display can be customized to show pre-designed things that the user chooses</li>
			<li>Programmable, it can be set to turn on/off, updating what is displayed is easy</li>
		</UL>
		
		<ExpandableImage src="/img/projects/coffee/sketch.png"
						 caption="The sketch I used to create the render."
						 />
		<P>
			I chose to use LED arrays for the screen, specifically medium sized LEDs rather than a fine TV grade LED screen because it would make replacing the screen very cheap and easy.
		</P>
		<P>
			The idea was comparable to how highway signs are being updated to be LEDs arrays
		</P>
		<SubHeading title="rendering it" name="rendering-it"/>
		<P>
			To render the vending machine, I fired up Autodesk Inventor, a program I hadn't touched since high school. It took me about a day to 3d model, add a decal onto it and place it in a 360 degree virtual environment with Autodesk Showcase.
		
		</P>
		<ExpandableImage right src="/img/projects/coffee/rendered.png" caption="The final rendering."/>
		
		
		
		
		
		
		<hr/>
		<Heading title="The App" name="the-design"/>
		<SubHeading title="why an app?" name="why-an-app"/>
		
		<P>
			Dan's insistence on minimal interaction partially influenced the idea for an app.
		</P>
		<P>		
			The second influence came from my summer. I had spent the summer playing around with designing my own Android app, so I thought, 
			<q>why not separate the interface into an app?</q>
			It wasn't stated anywhere that we couldn't do it.
			<br/>
			Over time, I started to realize the benefits of this idea: 
		</P>
		<UL>
			<li>
				<b>Unlimited time</b> to explore the app and create drinks
			</li>
			<li>
				Data-driven, <b>personalized experiences</b>
			</li>
			<li>
				Manage your accounts
			</li>
			<li>
				Accessibility and usability would be partially solved, consider:
			</li>
			<UL>
				<li>
					Smartphone users are likely to know how to use their own phones
				</li>
				<li>
					Smartphones already support accessibility options
				</li>
				<li>
					Google's Material Design guidelines guide well designed user interfaces
				</li>
			</UL>
		</UL>
		
		<SubHeading title="the first mockup: drink creator" name="the-first-mockup"/>
		
		<P>
			Due to the importance of customizability, the first mockup I created was on customizing a drink.
		</P>
		<P>
			You can start to see the previous influences that went into the design.
		</P>
		<ExpandableImage right floated
						 src="/img/projects/coffee/custom coffee animated.gif"
						 largeSrc="/img/projects/coffee/custom coffee animated large.gif"
						 caption="final version of customizing a drink" />

		<P>
			In particular note the following:
		</P>
		<UL>
			<li>
				<b>Fun</b>, the experience is designed to be played with
			</li>
			<li>
				The menu is designed <b>broad</b> and <b>shallow</b>, it invites exploration
			</li>
			<li>
				Indicators and graphics track progress
			</li>
			<li>
				The touch targets follow material design guideline sizes
			</li>
		</UL>
		<div className="clear"/>
		
		<SubHeading title="form design" name="form-design"/>
		
		<P>
			Forms would be important, and so it was the next piece to be designed. For the most part, I followed material design guidelines.
		</P>
		<ExpandableImage right floated
						 src="/img/projects/coffee/credit card form.png"
						 caption="a credit card form" />
		<P>

			Note the following:
		</P>
		<UL>
			<li>Instant feedback</li>
			<li>Error prevention through validation</li>
			<li>Helpful error messages</li>
			<li>
				Dynamic form that changes based on what’s selected
			</li>
			<li>
				Colorblindness-friendly color palette
			</li>			
		</UL>
		<div className="clear"/>
		
		<SubHeading title="color-blindess" name="color-blindness"/>
		
		<P>
			In fact, I made it a point to choose colors that were colorblindness friendly. The statistics on colorblindness are as follows:
		</P>
		<UL>
			<li>
				10% of the population is colorblind
			</li>
			<li>
				95% of the colorblind are <b>red-green colorblind</b> 
				meaning that most can't differentiate between red and green
			</li>
			<li>
				Only a very small proportion of the poulation is completely colorblind!
			</li>		
		</UL>
		<P>
			Everything is blue and orange shifted to deal with this
		</P>
		<ExpandableImage src="/img/projects/coffee/colorblindness.png"
						 largeSrc="/img/projects/coffee/colorblindness large.png"
						 caption="forms of color-blindness" />
		
		<SubHeading title="menu screen" name="menu-screen"/>
		
		<P>
			The second phase of mockups is where the data-driven, personalized, and fun aspects were really given a chance to shine.
		</P>
		
		<P>
			The first of these is the first menu screen that users would be dropped into, note how the design accomodates these concerns.
		</P>
		
		<ExpandableImage right floated
						 src="/img/projects/coffee/menu animate.gif"
						 largeSrc="/img/projects/coffee/menu animate large.gif"
						 caption="the landing page" />
		<UL>
			<li>
				Easy to explore for a new user
			</li>
			<li>
				Data-driven and advertises seasonal offerings
			</li>
			<li>
				Large, attractive, tappable images
			</li>
			<li>
				Expansion to display more information
			</li>
		</UL>
		
		<div className="clear"/>
		<P>
			This particular view is what one of our target user groups in the user analysis would see. She's someone the recommendation engine would identify as one interested in vegan products.
		</P>
		<P>
			Other features the menu would likely offer:
		</P>
		<UL>
			<li>
				search, sorting, and filtering
			</li>
			<li>
				interactive help, if the user partiicularly needs it
			</li>
		</UL>
		<P>
			Upon selecting a drink to customize, the user would be moved to the customization screen.
		</P>
		
		<SubHeading title="kiosk interfacing" name="kiosk-interfacing"/>
		
		<P>
			Here we have the screen the user would see when they need to start the NFC process:
		</P>
		
		<ExpandableImage right floated
						 src="/img/projects/coffee/NFC.gif"
						 largeSrc="/img/projects/coffee/NFC large.gif"
						 caption="interfacing through NFC" />
						 
		<P>
			Notice:
		</P>
		
		<UL>
			<li>
				Simple language
			</li>
			<li>
				Button to account for potential issues
			</li>
			<li>
				It's <b>fun</b> (hopefully)
			</li>
			<li>
				The user is congrat
			</li>
		</UL>
		<div className="clear"/>
		<P>
			In particular, the second screen will occur on the first time a user has had their drink processed. It features the following:
		</P>
		<UL>
			<li>
				First note, that the text is actually a <b>haiku</b>!
			</li>
			<li>
				The screen congratulates the user to appeal to their emotions.
			</li>
			<li>
				The various buttons are designed to entice tapping to encourage sharing
			</li>
			<li>
				There's also redundant buttons to account for any potential issues, to show that <i>we do care</i>
			</li>
		</UL>
		
		
		
		
		
		
		<hr/>
		<Heading title="Extras" name="the-basics"/>
		<SubHeading title="personalized cups" name="personalized-cups"/>
		
		<P>
			Remember Dan's insistence on having the process be as fast, easy, and simple as possible?
		</P>
		
		<P>
			To accomodate that, the team came up with the idea for connected reusable cups. This would also appeal to one of our user groups in the user analysis, the group concerned with the environment.
		</P>
		
		<ExpandableImage right floated
						 src="/img/projects/coffee/my cups.png"
						 caption="managing cups" />
						 
		<P>
			The cups would be embedded with NFC chips (these are really cheap, and biodegradable ones exist). The cup would be connected with the account. The user would manage their cups in the app, connecting them with a particular drink. Now, the user only needs to carry around their cup and these cups can be shared with others like how netflix accounts are shared between friends and family.
		</P>
		
		<P>
			There is a lot of logistics that would need to be handled in dealing with personalized cups, however, such as disposal and cup detection in the kiosk, so it would be an idea that would require a lot of testing.
		</P>
		
		<div className="clear"/>
		
		<P>
			More details about it can be found in the slides in the <a href="#fast-facts-deliverables">links section</a>
		</P>
				
				

				
		<hr/>
		<Heading title="The End" name="the-end"/>
		<SubHeading title="wrapping up" name="wrapping-up"/>
		
		<P>
			Overall in terms of my role in this project, I created all of the assets that were really the meat of our work/presentation. I worked with the team to come up with ideas and write-ups and a lot of other considerations.
		</P>
		
		<P>
			The slides contain much more information, especially in the comments of the slides, <a href="#fast-facts-deliverables">please check it out</a>.
		</P>
		
		
		<div className="emptySpace">
			&nbsp;
		</div>
	</Content>;
	
export default Coffee;