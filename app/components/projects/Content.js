import React from 'react';
const RRedux = require('react-redux');

import {
	SubHeading,
	Heading
}	from './SubHeading';

import { setProjectHeaders, setProjectCurrHeaderIdx } from '../../redux/reducers/ProjectReducer';



export var Content = React.createClass
({	
	getInitialState () 
	{
		return {
			show: false
		}
	},
	
	componentWillMount()
	{
		window.setTimeout(() => {
		var headers = [];
		var index = 0;
		var newProps = {};
		
		var newChildren = React.Children.map
		(
			this.props.children,
			child =>
			{
				if(child.type && 
				   child.type.displayName &&
				   child.type.displayName === SubHeading.displayName ||
				   child.type.displayName === Heading.displayName)
				{
					let type = child.type.displayName === Heading.displayName? "header" : "subheader";
					headers[index] = 
					{
						type:	type,
						name: 	child.props.name,
						title:	child.props.title
					}
					
					newProps = Object.assign({}, child.props, 
						{
							index:  index
						}
					);
					index++;
					return React.cloneElement(child, newProps);
				}
				else if(child.type && child.type === "hr")
				{
					headers[index] = 
					{
						type:	"hr"
					}
					index++;
				}
				return child;
			}
		)
		
		this.props.setHeadings(headers);
		
		this.newChildren = newChildren;
		}, 1000);
		
		window.setTimeout(() => this.setState({show: true}), 1000);
	},		
	
	componentDidMount()
	{
				
		
		window.setTimeout(() => this.props.setHeading(0), 1000);
	},
	
	newChildren: [],
	
	render()
	{
		return(<div className="narrativeText">{this.state.show? this.newChildren : null}</div>);
	}
	
});
Content = RRedux.connect(
	
	(s, ownProps) => 
	({	
		location: ownProps.location
	}),
	
	d =>
	({
		setHeadings(headers) { d(setProjectHeaders(headers)) },
		setHeading(index) { d(setProjectCurrHeaderIdx(index)); }
	})
	
)(Content);
	
export default Content;