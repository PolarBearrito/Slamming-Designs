import React from 'react';
const RRedux = require('react-redux');

var ReactDOM = require('react-dom');

var debounce = require('debounce');

import { setProjectCurrHeaderIdx } from '../../redux/reducers/ProjectReducer';

import './Navigator.less';
import './NavigatorFloating.less';

export class NavigatorFloating extends React.Component
{
	constructor(props)
	{
		super(props);
		
		this.state = 
		{
			expanded: false,
			baseHeight: 0,
			previewHeight: 0
		}

		this.adjust = this.adjust.bind(this);
		this.clipStyle = this.clipStyle.bind(this);
		
		this.render = this.render.bind(this);
	}
	
	componentDidMount()
	{
		this.setState
		({
			baseHeight: parseInt($(this.nav).outerHeight())			
		})
		
	}
	
	componentDidUpdate(prevProps)
	{
		if(prevProps.headers !== this.props.headers || this.state.previewHeight === 0 && this.content)
		{
			let height = parseInt(window.getComputedStyle(this.content).height);
			if(height > 0)
			{
				let prevHeight = height > 600? 200 : (height > 300? 100 : 60);
				window.setTimeout(() =>
				this.setState
				({
					previewHeight: prevHeight			
				}), 1);
			}
		}
	}
	
	adjust()
	{
		this.setState({ expanded: !this.state.expanded })
		
	}
	
	clipStyle()
	{
		if(this.state.expanded)
		{
			if(this.props.onChangeHeight)
			{
				window.setTimeout(() =>
				{
					this.props.onChangeHeight(this.state.baseHeight + parseInt(window.getComputedStyle(this.content).height));
				},10);
			}
			return { height: `${window.getComputedStyle(this.content).height}` }

		}
		
		else
		{
			if(this.props.onChangeHeight)
			{
				window.setTimeout(() =>
				{
					this.props.onChangeHeight(this.state.baseHeight + this.state.previewHeight);
				},1);
			}
			return { height: `${this.state.previewHeight}px` }
		}
	}
			
	
	render()
	{
		return(
			<div className="navigatorFloating" ref={thisElm => this.nav = thisElm}>
				<div className="title">
					<img src="/img/projects/contents/contents.png"/>
					<h2> Contents </h2>
				</div>
				
				<div className="contentClip" style={this.clipStyle()}>
					<div className="content" ref={thisElm => this.content = thisElm}>
				{				
					this.props.headers.map( 
						(header, index) => 
						{
							if(header.type === "hr")
								return <div className="navigatorHr" key={index}><hr/></div>
							else
							{
								let className = header.type === "header" ? "navigatorHead" : "navigatorEntry";
								return(
								<div className={className + 
											   (this.props.curHeaderIdx === index? " selected" : "")}
																
									 key={index}>
									<span>
										<a href={`#${header.name}`}
										   onClick={() => window.setTimeout(props.setHeading.bind(this, index), 10)}>
										{header.title}
										</a>
									</span>
								</div>
							)
							}
						}
					)
				}
					</div>
				</div>
				
				<div className="fastFactsFooter" onClick={this.adjust}>
					<img src="/img/projects/contents/down arrow.png" className={this.state.expanded? "flipped" : ""}/>
					<div>
					{this.state.expanded? "collapse":"expand"}
					</div>
				</div>
				
			</div>
		);
		
		
	}
	
}
NavigatorFloating = RRedux.connect(
	
	s => 
	({	
		headers: 		s.Project.headers,
		curHeaderIdx: 	s.Project.curHeaderIdx
	}),
	
	d => 
	({	
		setHeading(index) { d(setProjectCurrHeaderIdx(index)); }
	})
	
)(NavigatorFloating);



export var Navigator = React.createClass
({

	getInitialState()
	{
		return (
		{ 
			headerWidths: 	[],
			headerHeights: 	[]
			
		});
	},
	
	node: "",
	
	componentDidMount()
	{
		this.node = ReactDOM.findDOMNode(this.navigator);
		
		window.addEventListener("scroll", this.scrollWait);
	},
	
	scrollWait()
	{
		var viewportOffset = this.node.getBoundingClientRect();
		// these are relative to the viewport
		var top = viewportOffset.top;
		
		var marginTop = parseInt(this.node.style.marginTop, 10) || 0;
		
		var supportPageOffset = window.pageXOffset !== undefined;
		var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");

		var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
		
		if(top < 20)
		{
			var off = 20 - top;
			
			//console.log("off: " + off);
			
			
			
			this.scroll = y  - off
			//console.log("scrollY: " + window.scrollY);
			//console.log("scroll: " + this.scroll);
			//console.log(window.scrollY);
			
			window.addEventListener("scroll", this.scrollListen);
			
			this.node.style.top = "20px";
			this.node.style.position = "fixed";
			this.node.style.width = "162.667px";
			
			window.removeEventListener("scroll", this.scrollWait);
		}
		
		//console.log(top);	
		//console.log(marginTop + (-top) + 50 + "px");	
	},
	
	scrollListen()
	{
		//this.setNavigatorMarginTop.bind(this, Math.max(window.pageYOffset  - this.scroll, 0) + "px")();
		if(window.pageYOffset <= this.scroll)
		{
			this.node.style.top = "0px";
			this.node.style.position = "relative";
			this.node.style.width = "auto";
			
			window.removeEventListener("scroll", this.scrollListen);
			window.addEventListener("scroll", this.scrollWait);
		}
			
		
		//console.log(window.pageYOffset  - this.scroll + "px");
		//console.log(window.pageYOffset);
		//console.log(this.scroll);
	},
	
	setNavigatorMarginTop(value)
	{
		this.node.style.marginTop = value;
		//console.log("hi");
	},
	
	componentDidUpdate(prevProps, prevState)
	{
		if(prevProps === this.props)
			return;

		// headerHeights.length === 0 for when go from mobile -> non-mobile and vice-versa IIRC
		if(prevProps.headers !== this.props.headers || this.state.headerHeights.length === 0)
			if(this.navHeaders.children.length > 0)
			{
				const children = Array.prototype.slice.call( this.navHeaders.children );
				var limit = this.navHeaders.offsetWidth - 20;
				
				var widths = [];
				var heights = [];
				
				children.forEach
				(
					(header, index) => 
					{
						heights[index] 	= parseFloat(window.getComputedStyle(header).height)
										+ parseFloat(window.getComputedStyle(header).paddingBottom)
										+ parseFloat(window.getComputedStyle(header).paddingTop);
						widths[index] 	= header.children[0] ? Math.min(header.children[0].offsetWidth, limit) : limit;
					}
				);	
				
				this.setState
				({
					headerWidths: 	widths,
					headerHeights: 	heights
				});
			}
	},
	
	componentWillUnmount()
	{
		window.removeEventListener("scroll", this.scrollListen);
		window.removeEventListener("scroll", this.scrollWait);
	},
	
	arrowStyle()
	{
		var top=  (this.state.headerHeights.slice(0, this.props.curHeaderIdx).reduce
			  ( 
				  (agg,curr) => agg + curr,
				  0
			  ) + this.state.headerHeights[this.props.curHeaderIdx]/2 - 8 + "px");
		var left= (this.state.headerWidths[this.props.curHeaderIdx] + 20 + "px");
		
		var prevLeft = this.arrow && parseInt(this.arrow.style.marginRight, 10) || 0;
		var movingLeft = prevLeft < parseInt(left, 10);
		
		//console.log(movingLeft);
		return(
		{
			marginTop: 		top,
			marginRight: 	left,
			transitionProperty:	movingLeft? "margin-top, margin-right" : "margin-right, margin-top",
			transitionDuration: ".2s, .2s",
			transitionTimingFunction: "ease-in-out, ease-in-out",
			transitionDelay: ".2s, 0s"
							
		})
	},
		
	render()
	{
		const props = this.props;
		return(
			<div className="navigator" ref={thisElm => this.navigator = thisElm}>
				<div className="navigatorTitle">Contents</div>
				<div className="arrow" style={this.arrowStyle()} ref={thisElm => this.arrow = thisElm}>
					>
				</div>
				<div ref={thisElm => this.navHeaders = thisElm}>
				{				
					props.headers.map
					( 
						(header, index) => 
						{
							if(header.type === "hr")
								return <div className="navigatorHr" key={index}><hr/></div>
							else
							{
								let className = header.type === "header" ? "navigatorHead" : "navigatorEntry";
								return(
								<div className={className + 
											   (props.curHeaderIdx === index? " selected" : "")}
																
									 key={index}>
									<span>
										<a href={`#${header.name}`}
										   onClick={() => window.setTimeout(props.setHeading.bind(this, index), 10)}>
										{header.title}
										</a>
									</span>
								</div>
							)
							}
						}
					)
				}
				</div>
			</div>
		);
	}
});
Navigator = RRedux.connect(
	
	s => 
	({	
		headers: 		s.Project.headers,
		curHeaderIdx: 	s.Project.curHeaderIdx
	}),
	
	d => 
	({	
		setHeading(index) { d(setProjectCurrHeaderIdx(index)); }
	})
	
)(Navigator);

export default Navigator;