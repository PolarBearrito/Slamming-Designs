import React from 'react';
import DisplayIf from "./DisplayIf";

export var ExpandableImage = React.createClass
({
	expand() 
	{ 
		this.overlay.style.visibility = "visible"; 
		this.overlay.style.opacity = 1; 
		this.overlayImg.src = this.overlayImg.dataset.src;
	},
	
	hide() 
	{ 
		this.overlay.style.visibility = "hidden"; 
		this.overlay.style.opacity = 0; 
	},
	
	flip()
	{
		window.setTimeout( () =>
		{
			this.loadingImg.style.opacity = "0";
			this.overlayImg.onload = "";
		}, 10);
		
		window.setTimeout( () =>
		{
			this.loadingImg.style.visibility = "hidden";
		}, 200);
	},
	
	componentDidMount()
	{
		this.overlayImg.onload = this.flip;
	},
	
	render()
	{
		return(
			<div className={`image${this.props.right? " pulledRight" : ""}${this.props.floated? " floated" : ""}`}>
				<div className="imageWrapper">
					<img src={this.props.src} onClick={this.expand}/>
					<DisplayIf if={this.props.right}>
						<div className="caption">
							{this.props.caption}
						</div>
					</DisplayIf>
				</div>
				<DisplayIf if={!this.props.right}>
					<div className="caption">
						{this.props.caption}
					</div>
				</DisplayIf>
				<div className="overlayWrapper" 
					 ref={thisElm => this.overlay = thisElm}
					 onClick={this.hide}>
					<div className="backgroundImage"/>
					<img ref={thisElm => this.overlayImg = thisElm}
						 data-src={this.props.largeSrc || this.props.src}/>
					<div className="spinner" 
					     ref={thisElm => this.loadingImg = thisElm}>
						<i className="fa fa-spinner" 
						   aria-hidden="true"/>
					</div>
				</div>
			</div>
		)
	}
});

export default ExpandableImage;