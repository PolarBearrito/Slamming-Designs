const React = require('react');
const RRedux = require('react-redux');

import Content from './Content';
import {
	SubHeading,
	Heading
} from './SubHeading';
import ExpandableImage from "./ExpandableImage";



export var Dopamine = props =>
	<Content>
	
		<Heading title="The Basics" name="the-basics"/>
		<SubHeading title="working on it..." name="working-on-it"/>
		<p>
			I'm working on the write-up.
		</p>
		
		<p>
			However, the <a href="#fast-facts-deliverables">associated document</a> is quite self-explanatory. The write-up that would appear here would be an expansion on the motivations and events leading up to the production of the document.
		</p>
		
		<div className="emptySpace">
			&nbsp;
		</div>
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>		
		<div className="emptySpace">
			&nbsp;
		</div>
	</Content>;
	
export default Dopamine;