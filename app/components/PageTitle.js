import React from 'react';
const RRedux = require('react-redux');

import { changeTitle } from '../redux/reducers/PageTitleReducer';

var PageTitle = React.createClass
({
    componentDidUpdate(prevProps, prevState)
	{
		if(prevProps.title !== this.title)
		{
			this.props.changeTitle(this.props.title);
		}
	},
	
	render()
    {
        window.document.title = `Steven Lam: ${this.props.title}`;

        return (null);
    }
});
PageTitle = RRedux.connect(
	
	s => 
	({
	}),
	
	d =>
	({
		changeTitle: (title) => d(changeTitle(title))
	})
	
)(PageTitle);

export default PageTitle;