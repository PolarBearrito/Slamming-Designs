import React from 'react';
const RRedux = require('react-redux');

import { PAGES, changePageRoute } from '../redux/reducers/NavigationReducer';
import { setEntered } from '../redux/reducers/IntroReducer';
import Intro from './Intro.js';

import cookie from 'react-cookie';
var ReactGA = require('react-ga');

var Waypoint = require('react-waypoint');

var MediaQuery = require('react-responsive');

var Footer = React.createClass
({
	getInitialState()
	{
		return ({ visible: false	});
	},
	
	render()
	{
		return(
			<div>
				<Waypoint bottomOffset="-300px" 
						  onEnter={() => {this.setState({visible: true})}}
						  onLeave={() => {this.setState({visible: false})}}
				>
					<div className="footerWindow"/>
				</Waypoint>
				<div className="footer"
					style={{visibility: this.state.visible? "visible" : "hidden"}}>
					<img src="/img/footer.png"/>
				</div>
			</div>
		)
	}
});

import './NavigationBar.less';
	
var NavigationBar = props =>
	<div className="navBar">
		<div className="page">
			<img className="navLogo clickable" 
			     src="/img/nav-logo.png"
				 onClick={ e => props.setPortfolio() }/>
			
			<a className={(props.contactSelected? "selected" : "") + " navLink clickable"}
			   onClick={ e => props.setContact() }>Contact</a>

			<a className={(props.aboutSelected? "selected" : "") + " navLink clickable"}
			   onClick={ e => props.setAbout() }>About</a>
			
			<a className={(props.portfolioSelected? "selected" : "") + " navLink clickable"}
			   onClick={ e => props.setPortfolio() }>Portfolio</a>
			   
		</div>
	
	</div>;
	
var Hamburger = props =>
	<div className="hamburger">
		<div className="line"/>
		<div className="line"/>
		<div className="line"/>
	</div>;
	
class NavigationBarMobile extends React.Component
{
	constructor(props)
	{
		super(props);
		
		this.state =
		{
			expanded: false,
		}
		
		// bind this
		this.toggle 		= this.toggle.bind(this);
		this.collapse 		= this.collapse.bind(this);
		this.setPortfolio 	= this.setPortfolio.bind(this);
		this.setAbout 		= this.setAbout.bind(this);
		this.setContact 	= this.setContact.bind(this);
		
		this.render = () =>
			<div className={(this.state.expanded? "expanded " : "") + "navBarMobileClip"}>
				<div className="overlay" 
		style={{ display: this.state.expanded? "block" : "none"}}
					onClick={this.collapse}/>
				<div className="navBarMobile">
					<a className="navLogoPage" onClick={e => this.toggle()}>
						<div className="navHamburger">
							<Hamburger/>
						</div>
						<img className="navLogo" 
							 src="/img/nav-logo.png"
							 onClick={ e => this.props.setPortfolio() }/>
						<div className="navSelectedPage">
							{this.props.selectedPageText}
						</div>

					</a>
					<div className="navLinks">
						<div className={(this.props.portfolioSelected? "selected" : "") + " navLink clickable"}
						onClick={ e => this.setPortfolio() }>Portfolio</div>
						<div className={(this.props.aboutSelected? "selected" : "") + " navLink clickable"}
						onClick={ e => this.setAbout() }>About</div>
						<div className={(this.props.contactSelected? "selected" : "") + " navLink clickable"}
						onClick={ e => this.setContact() }>Contact</div>
					</div>
					<div className="navFooter">
						<div>
							<span className="copy">&copy;</span> Steven Lam
						</div>
						<div>
						You can see the code for this site&nbsp;	
						<ReactGA.OutboundLink
								eventLabel="gitlab"
								target="_self"
								to="https://gitlab.com/PolarBearrito/Slamming-Designs">
								on my Gitlab!
						</ReactGA.OutboundLink>
						</div>
						<div>
							(site last updated: 2/2/21)
						</div>
					</div>
				</div>
			</div>;
	}
	
	toggle()
	{
		this.setState({expanded: !this.state.expanded});
	}
	
	collapse()
	{
		this.setState({expanded: false});
	}
	
	setPortfolio()
	{
		this.props.setPortfolio();
		this.collapse();
	}
	
	setAbout()
	{
		this.props.setAbout();
		this.collapse();
	}
	
	setContact()
	{
		this.props.setContact();
		this.collapse();
	}
	
}

var NavigationBarConnect = RRedux.connect(
	
	s => 
	({
		portfolioSelected: 	s.Navigation.page === PAGES.PORTFOLIO,
		aboutSelected: 		s.Navigation.page === PAGES.ABOUT,
		contactSelected: 	s.Navigation.page === PAGES.CONTACT,
	}),
	
	d => 
	({	
		setPortfolio() 	
		{ 
			d(changePageRoute(PAGES.PORTFOLIO));
		},
		setAbout() 		
		{ 
			d(changePageRoute(PAGES.ABOUT));
		},
		setContact() 	
		{ 
			d(changePageRoute(PAGES.CONTACT));
		}
	}),
	
	(s, d) => Object.assign({}, s, d, 
	{
		selectedPageText: s.portfolioSelected? "portfolio" : 
						  s.aboutSelected? "about" :
						  s.contactSelected? "contact" : ""
				
	})
	
);

NavigationBar = NavigationBarConnect(NavigationBar);
NavigationBarMobile = NavigationBarConnect(NavigationBarMobile);

		
				

var Layout = React.createClass(
{
	componentWillMount()
	{
		var entered = cookie.load("entered") || 0;
		if(entered)
			this.props.setEntered();
	},
	
	render: function()
	{
		window.React = React;
		return (
		<div id="root">
			{this.props.entered?
				""
				:
				<Intro/>
			}
			
		<MediaQuery query='(min-width: 601px)'>
			<div className="centered">
				<span>
				You can see the code for this site&nbsp;<ReactGA.OutboundLink
					eventLabel="gitlab"
					target="_self"
					to="https://gitlab.com/PolarBearrito/Slamming-Designs">
					on my Gitlab!
				</ReactGA.OutboundLink>
				&nbsp;(site last updated: 2/2/21)
				</span>
			</div>
		</MediaQuery>
		<MediaQuery query='(min-width: 601px)'>
			<NavigationBar/>
		</MediaQuery>
		<MediaQuery query='(max-width: 600px)'>
			<NavigationBarMobile/>
		</MediaQuery>
			
			{this.props.children}
			<Footer/>
		</div>);
	}
});
Layout = RRedux.connect(
	
	s => 
	({
		entered: s.Intro.entered
	}),
	
	d =>
	({
		setEntered() { d(setEntered()) }
	})
	
)(Layout);



	
export default Layout;