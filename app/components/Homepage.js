import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { browserHistory } from 'react-router';

import Intro from './Intro.js';



var Homepage = React.createClass(
{	
	componentWillMount()
	{
		const props = this.props;
		
		if(props.entered)
			props.skipToAbout()
	},
	
	componentDidUpdate(prevProps)
	{
		const props = this.props;
		
		if(prevProps !== props && props.entered)
			props.skipToAbout()
	},
	
	render()
	{
		return(null);
	}
});
Homepage = RRedux.connect(
	
	s => 
	({
		entered: s.Intro.entered
	}),
	
	d =>
	({
		skipToPortfolio() {	browserHistory.push("/portfolio");},
		skipToAbout() 	{	browserHistory.push("/about");}
	})
	
)(Homepage);




export default Homepage;
	