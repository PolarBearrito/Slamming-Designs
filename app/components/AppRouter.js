// src/components/AppRoutes.js
import React from 'react';
import { Router, browserHistory } from 'react-router';
import routes from '../routes';

var ReactGA = require('react-ga');
ReactGA.initialize('UA-97071913-1');
 
function logPageView() 
{
  ReactGA.set({ page: window.location.pathname });
  ReactGA.pageview(window.location.pathname);
} 

export default class AppRouter extends React.Component 
{
  render() 
  {
    //console.log(routes);
	return (
		<Router 
			history={this.props.history} 
			routes={routes} 
			onUpdate={() => 
				{
					
					logPageView();
				}
			}/>
    );
  }
}