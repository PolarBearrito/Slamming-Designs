import React from 'react';
import Redux from 'redux';
import { browserHistory } from 'react-router';

import { changeCategory } from './PortfolioReducer';



export const PAGES = 
{
	PORTFOLIO: 	'portfolio',
	ABOUT:		'about',
	CONTACT:	'contact'	
}

const ROUTES = 
{
	[PAGES.PORTFOLIO]: 	"/portfolio",
	[PAGES.ABOUT]: 		"/about",
	[PAGES.CONTACT]: 	"/contact",
}

const INITS = 
{
	[PAGES.PORTFOLIO]:	dispatch => { dispatch(changeCategory(""))},
	[PAGES.ABOUT]:		dispatch => {},
	[PAGES.CONTACT]:	dispatch => {}
}
	

export const CHANGE_PAGE = 'CHANGE_PAGE'
export function changePageRoute(page) 
{
	if(page === PAGES.PORTFOLIO)
		window.scroll(0,0);
	
	return function (dispatch) 
	{  
		dispatch(changePage(page));
		browserHistory.push(ROUTES[page]);
		dispatch(INITS[page]);
	}

}

export function changePage(page) 
{
	return {
		type: CHANGE_PAGE,
		page
	}
}



// The Reducer Function
const NavigationReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			page: ''
		};
	}

	switch(action.type) 
	{
		case 'CHANGE_PAGE':
			return Object.assign({}, state, { page: action.page });
	}
	return state;
}

export default NavigationReducer