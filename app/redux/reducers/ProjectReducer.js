import React from 'react';
import Redux from 'redux';

import { PROJECTS, PROJECTS_DATA } from "./Projects.js";
import { changeCategory } from "./PortfolioReducer.js";

import { browserHistory } from 'react-router';

export const generateActionCreator = (type, ...keys) => 
{
    return function(...vals)
	{
		let action = {type};
		keys.map( 
			(key, index) => action[keys[index]] = vals[index] 
		);
		return action;
	}
}

const SET_PROJECT = 'SET_PROJECT'
export function setProjectRoute(category, project)
{
	return function (dispatch) 
	{  
		if(category !== "" && project !== "")
		{
			dispatch(changeCategory(category));
			dispatch(setProject(project));
			var route = "/portfolio/" + category + "/" + project;
			return browserHistory.push(route);
		}
		return browserHistory.push("/portfolio");
	}	
}
export function setProject(project) 
{
	return function (dispatch) 
	{  
		dispatch(setProjectName(project));
		
		//console.log(PROJECTS_DATA);
		
		var data = PROJECTS_DATA[project];
		
		if(data)
		{
			dispatch(setProjectColor(data.color));
			dispatch(setProjectImage(data.image));
			dispatch(setProjectTitle(data.title));
			dispatch(setProjectDescription(data.description));
			
			dispatch(setProjectWhen(data.when));
			dispatch(setProjectDuration(data.duration));
			
			dispatch(setProjectIngrdients(data.ingredients));
			
			dispatch(setProjectIndividual(data.individual));
			dispatch(setProjectOthers(data.others));
			dispatch(setProjectRoles(data.roles));
			dispatch(setProjectClass(data.class));
			
			dispatch(setProjectTags(data.tags));
			dispatch(setProjectDeliverables(data.deliverables));

			dispatch(setProjectNarrative(data.narrative));
		}
	}
}
export const setProjectName 		= generateActionCreator(SET_PROJECT, 'project'); 

export function decreaseHeader(project) 
{
	return function (dispatch, getState) 
	{  
		var curHeaderIdx = getState().curHeaderIdx;
		var headers = getState().headers;
		
		for(var i = curHeaderIdx-1; i >= 0; i--)
		{
			if(headers[i].type !== "hr")
			{
				return dispatch(setProjectCurrHeaderIdx(i));
			}
		}
	}
}

export function increaseHeader(project) 
{
	return function (dispatch, getState) 
	{  
		var curHeaderIdx = getState().curHeaderIdx;
		var headers = getState().headers;
		
		for(i = curHeaderIdx+1; i < headers.length; i++)
		{
			if(headers[i].type !== "hr")
			{
				return dispatch(setProjectCurrHeaderIdx(i));
			}
		}
	}
}



const SET_HEADERS = 'SET_HEADERS'
export const setProjectHeaders 		= generateActionCreator(SET_HEADERS, 'headers');
const SET_CURRENT_HEADER_INDEX = 'SET_CURRENT_HEADER_INDEX'
export const setProjectCurrHeaderIdx= generateActionCreator(SET_CURRENT_HEADER_INDEX, 'index');


const SET_COLOR = 'SET_COLOR'
export const setProjectColor		= generateActionCreator(SET_COLOR, 'color');
const SET_IMAGE = 'SET_IMAGE'
export const setProjectImage 		= generateActionCreator(SET_IMAGE, 'image');
const SET_TITLE = 'SET_TITLE'
export const setProjectTitle 		= generateActionCreator(SET_TITLE, 'title');
const SET_DESCRIPTION = 'SET_DESCRIPTION'
export const setProjectDescription 	= generateActionCreator(SET_DESCRIPTION, 'description');


const SET_WHEN = 'SET_WHEN'
export const setProjectWhen 		= generateActionCreator(SET_WHEN, 'when');
const SET_DURATION = 'SET_DURATION'
export const setProjectDuration 	= generateActionCreator(SET_DURATION, 'duration');


const SET_INGREDIENTS = 'SET_INGREDIENTS'
export const setProjectIngrdients 	= generateActionCreator(SET_INGREDIENTS, 'ingredients');

const SET_INDIVIDUAL = 'SET_INDIVIDUAL'
export const setProjectIndividual 	= generateActionCreator(SET_INDIVIDUAL, 'individual');
const SET_OTHERS = 'SET_OTHERS'
export const setProjectOthers 		= generateActionCreator(SET_OTHERS, 'others');
const SET_ROLES = 'SET_ROLES'
export const setProjectRoles 		= generateActionCreator(SET_ROLES, 'roles');
const SET_CLASS = 'SET_CLASS'
export const setProjectClass 		= generateActionCreator(SET_CLASS, 'class');

const SET_TAGS = 'SET_TAGS'
export const setProjectTags 		= generateActionCreator(SET_TAGS, 'tags');
const SET_DELIVERABLES = 'SET_DELIVERABLES'
export const setProjectDeliverables 	= generateActionCreator(SET_DELIVERABLES, 'deliverables');

const SET_NARRATIVE = 'SET_NARRATIVE'
export const setProjectNarrative	= generateActionCreator(SET_NARRATIVE, 'narrative');



// The Reducer Function
const ProjectReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			project: 		"",
			
			headers: 		[],
			curHeaderIdx: 	0,
			
			color: 			"",
			image: 			"",
			title: 			"",
			description: 	"",
			
			when: 			"",
			duration: 		"",
			ingredients: 	[],
			individual: 	true,
			others: 		[],
			roles: 			[],
			class: 			"",
			
			tags: 			[],
			deliverables: 	[],			
			
			narrative: 		""		
		};
	}

	switch(action.type) 
	{
		case SET_PROJECT:
			return Object.assign({}, state, { project: 		action.project });
			
		case SET_HEADERS:
			return Object.assign({}, state, { headers: 		action.headers });
		case SET_CURRENT_HEADER_INDEX:
			return Object.assign({}, state, { curHeaderIdx: action.index });
			
		case SET_COLOR:
			return Object.assign({}, state, { color: 		action.color });
		case SET_IMAGE:
			return Object.assign({}, state, { image: 		action.image });
		case SET_TITLE:
			return Object.assign({}, state, { title: 		action.title });
		case SET_DESCRIPTION:
			return Object.assign({}, state, { description: 	action.description });
			
		case SET_WHEN:
			return Object.assign({}, state, { when: 		action.when });
		case SET_DURATION:
			return Object.assign({}, state, { duration: 	action.duration });
			
		case SET_INGREDIENTS:
			return Object.assign({}, state, { ingredients: 	action.ingredients });
		case SET_INDIVIDUAL:
			return Object.assign({}, state, { individual: 	action.individual });
		case SET_OTHERS:
			return Object.assign({}, state, { others: 		action.others });
		case SET_ROLES:
			return Object.assign({}, state, { roles: 		action.roles });
		case SET_CLASS:
			return Object.assign({}, state, { class: 		action.class });
		case SET_TAGS:
			return Object.assign({}, state, { tags: 		action.tags });
		case SET_DELIVERABLES:
			return Object.assign({}, state, { deliverables: action.deliverables });
			
		case SET_NARRATIVE:
			return Object.assign({}, state, { narrative: 	action.narrative });
	}
	return state;
}

export default ProjectReducer