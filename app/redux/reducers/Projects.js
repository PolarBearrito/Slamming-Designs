import { Coffee } from "../../components/projects/Coffee";
import { Lamp } from "../../components/projects/Lamp";
import { Dopamine } from "../../components/projects/Dopamine";
import { Kaomoji } from "../../components/projects/Kaomoji";
import { u } from "../../helpers/toUnicode";



export const PROJECTS =
{
	COFFEE: 	"coffee-vending-machine-android-app",
	LAMP: 		"antibiotic-acne-lamp",
	DOPAMINE: 	"dopamine-and-SSRIs",
	KAOMOJI: 	"kaomoji-community-project",




};

export const PROJECTS_NARRATIVES = 
{
	[PROJECTS.COFFEE]: 		Coffee,
	[PROJECTS.LAMP]: 		Lamp,
	[PROJECTS.DOPAMINE]: 	Dopamine,
	[PROJECTS.KAOMOJI]:		Kaomoji,
};


export const PROJECTS_DATA =
{
	"" :
	{
		color: 			"",
		image: 			"",
		title: 			"",
		description: 	"",
		
		when: 			"",
		duration: 		"",
		ingredients: 	[],
		individual: 	true,
		others: 		[],
		roles: 			[],
		class: 			"",
		tags: 			[],
		deliverables: 	[]
	},
	
	[PROJECTS.COFFEE] : 
	{
		color: 			"#795548",
		image: 			"/img/projects/coffee/coffee-hero.png",
		title: 			"Coffee Vending Machine Android App",
		description: 	"Product and UI/UX design merge to create a coffee product vending machine concept and its app.",
		
		when: 			"Fall 2015",
		duration: 		"2 months, mockups/renders: 4 days",
		
		ingredients: 	
		[
			"Adobe Illustrator",
			"Adobe After Effects",
			"Autodesk Inventor",
			"Autodesk Showcase"
		],
		individual: 	false,
		others: 		
		[
			"Dan Gopstein",
			"Michael Chung",
			"Matthew Pon"
		],
		roles: 			
		[
			"Product Designer",
			"Sole 3D Modeler",
			"Sole UI & UX Designer"
		],
		class: 			"Human Computer Interface",
		tags: 			
		[
			"product design",
			"UI design",
			"UX design",
			"Adobe Illustrator",
			"Adobe After Effects",
			"Android",
			"App",
			"3D Modeling"
		],
		deliverables: 	
		[
			{ 
				text: "Final Presentation Slides",
				label:"coffee presentation",
				link: "https://docs.google.com/presentation/d/1LVSM6op0pglv-1o0ZQAvBrau6u1RFmDwo1dBxKInYB4/edit?usp=sharing",
			}
		]
	},
	
	[PROJECTS.LAMP] : 
	{
		color: 			"#69F0AE",
		image: 			"/img/projects/lamp/lamp-hero.png",
		title: 			`Antibiotic ${u("00b5")}controller-backed Timed Blue Acne Lamp`,
		description: 	"By merging electrical engineering and computer engineering, I build an antibiotic blue LED lamp prototype and program a multi-threaded embedded system for the microcontroller controlling it.",
		
		when: 			"Fall 2015",
		duration: 		"2 weeks",
		
		ingredients: 	
		[
			"STM32F4 Discovery Microcontroller Board",
			"Kiel \f00B5Vision4 IDE",
			"Blue LEDs",
			"Breadboards",
			"Wiring",
			"PING Ultrasonic Distance Sensor",
			"Push Buttons",
			"Programmable LED Screen",
			"Transistors",
			"Misc. Hardware"
		],
		individual: 	true,
		others: [],
		roles: 			
		[
			"Product Designer",
			"Software Developer",
			"Electrical Engineer",
			"Computer Engineer"
		],
		class: 			"Embedded Systems",
		tags: 			
		[
			"hardware",
			"electrical engineering",
			"computer engineering",
			"embedded systems",
			"C",
			"multi-threaded",
			"parallel programming",
			"LEDs"
		],
		deliverables: 	
		[
			{ 
				text: "Final Submission Write-Up (40+ pages)",
				label:"lamp doc",
				link: "https://drive.google.com/file/d/0Bx8ZA7wbMsXXRHlBR0lpbkl1dlk/view?usp=sharing",
			}
		]
	},
	
	[PROJECTS.DOPAMINE] : 
	{
		color: 			"#80DEEA",
		image: 			"/img/projects/dopamine/dopamine-hero.png",
		title: 			"The Role of Dopamine in the Mechanism of Action of SSRIs for Depression",
		description: 	"I compile compelling evidence across multiple studies that begin to highlight the essential role dopamine plays in the dysfunctions of depression and advocate for novel therapies that target dopamine.",
		
		when: 			"Spring 2015",
		duration: 		"1 week",
		
		ingredients: 	
		[
			"Microsoft Word",
			"Academic Access to Medical Journals"
		],
		individual: 	true,
		others: [],
		roles: 			
		[
			"Researcher",
			"Writer"
		],
		class: 			"Human Computer Interface",
		tags: 			
		[
			"medicine",
			"psychology",
			"abnormal psychology",
			"depression",
			"dopamine",
			"writing",
			"research paper",
			"biology",
			"neurology",
			"science"
		],
		deliverables: 	
		[
			{ 
				text: "Dopamine and Depression",
				label:"dopamine doc",
				link: "https://drive.google.com/file/d/0Bx8ZA7wbMsXXYXlhZGl4Q1BRX0E/view?usp=sharing",
			}

		]
	},
	[PROJECTS.KAOMOJI] : 
	{
		color: 			"#40c4ff",
		image: 			"/img/projects/kaomoji/kaomoji-hero.png",
		title: 			"Kaomoji Community Site App Capstone Project",
		description: 	"A culmination of software development, computer science, software documentation, project management and software architecture results in this final capstone project.",
		
		when: 			"Fall 2016",
		duration: 		"3 months, code: 1.5 weeks",
		
		ingredients: 	
		[
			"Ubuntu Linux Server(s)",
			"Node.js",
			"Express.js",
			"React.js",
			"React.js Redux",
			"Webpack",
			"Silicon C++ Framework",
			"Microhttpd Server",
			"MySQL"
		],
		individual: 	false,
		others: 
		[
			"Masato Anzai",
			"Jonathan Allbritten",
			"Patrick Kingchatchaval"
		],
		roles: 			
		[
			"Project Lead",
			"Project Manager",
			"Software Architect",
			"Sole Front-End Developer",
			"Sole Back-End Developer",
			"UI/UX Designer",
			"Project Designer"
		],
		class: 			"Design Project",
		tags: 			
		[
			"software",
			"front-end",
			"back-end",
			"product design",
			"project management",
			"software architecture",
			"UI/UX design",
			"kaomoji",
			"code",
			"programming",
			"JS",
			"C++",
			"MySQL",
			"React.js",
			"React.js Redux",
			"Node.js",
			"Express.js",
			"Silicon C++ Framework"
		],
		deliverables: 	
		[
			{ 
				text: "Final Project Presentation",
				label:"kaomoji presentation",
				link: "https://docs.google.com/presentation/d/12yWydrqKnOYD3byu5e1DLJVhupatwXmx2g0XIw6rHaw/edit?usp=sharing",
			},
			{ 
				text: "Project Demo",
				label:"kaomoji demo",
				link: "http://kaomoji.slammingdesigns.com",
			},
			{ 
				text: "Project Code Repository",
				label:"kaomoji repo",
				link: "https://gitlab.com/PolarBearrito/kaomoji-project",
			},
		]
	},
};

export default PROJECTS_DATA