import React from 'react';
import Redux from 'redux';
import { browserHistory } from 'react-router';



export const CATEGORIES = 
{
	DESIGN: 	'design',
	FRONTEND: 	'frontend',
	BACKEND: 	'backend',
	HARDWARE: 	'hardware',
	WRITING: 	'writing'
}

const ROUTES = 
{
	[CATEGORIES.DESIGN]: 	"/portfolio/design",
	[CATEGORIES.FRONTEND]: 	"/portfolio/frontend",
	[CATEGORIES.BACKEND]: 	"/portfolio/backend",
	[CATEGORIES.HARDWARE]: 	"/portfolio/hardware",
	[CATEGORIES.WRITING]: 	"/portfolio/writing"
}

export const NAMES = 
{
	[CATEGORIES.DESIGN]: 	"Design",
	[CATEGORIES.FRONTEND]: 	"Front End",
	[CATEGORIES.BACKEND]: 	"Back End",
	[CATEGORIES.HARDWARE]: 	"Hardware",
	[CATEGORIES.WRITING]: 	"Writing"
}

export const CHANGE_CATEGORY = 'CHANGE_CATEGORY'
export function changeCategoryRoute(category) 
{
	return function (dispatch) 
	{  
		dispatch(changeCategory(category));
		if(category !== "")
			return browserHistory.push(ROUTES[category]);
		return browserHistory.push("/portfolio");
	}

}
export function changeCategory(category) 
{
	return {
		type: CHANGE_CATEGORY,
		category
	}
}



// The Reducer Function
const PortfolioReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			category: ''
		};
	}

	switch(action.type) 
	{
		case 'CHANGE_CATEGORY':
			return Object.assign({}, state, { category: action.category });
	}
	return state;
}

export default PortfolioReducer