import React from 'react';
import Redux from 'redux';

import { browserHistory } from 'react-router';

export const generateActionCreator = (type, ...keys) => 
{
    return function(...vals)
	{
		let action = {type};
		keys.map( 
			(key, index) => action[keys[index]] = vals[index] 
		);
		return action;
	}
}


const SET_FAQS = 'SET_FAQS'
export const setAboutFAQs 		= generateActionCreator(SET_FAQS, 'FAQs');
const SET_CURRENT_FAQS_INDEX = 'SET_CURRENT_FAQS_INDEX'
export const setAboutCurrFAQsIdx= generateActionCreator(SET_CURRENT_FAQS_INDEX, 'index');





// The Reducer Function
const AboutReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			FAQs: 		[],
			curFAQsIdx: 	0,

		};
	}

	switch(action.type) 
	{
			
		case SET_FAQS:
			return Object.assign({}, state, { FAQs: 		action.FAQs });
		case SET_CURRENT_FAQS_INDEX:
			return Object.assign({}, state, { curFAQsIdx: action.index });
		
	}
	return state;
}

export default AboutReducer