import React from 'react';
import Redux from 'redux';



export const CHANGE_TITLE = 'CHANGE_TITLE'
export function changeTitle(title) 
{
	return {
		type: CHANGE_TITLE,
		title
	}
}



// The Reducer Function
const PageTitleReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			title: ''
		};
	}

	switch(action.type) 
	{
		case 'CHANGE_TITLE':
			return Object.assign({}, state, { title: action.title });
	}
	return state;
}

export default PageTitleReducer