import React from 'react';
import Redux from 'redux';
import cookie from 'react-cookie';

import { browserHistory } from 'react-router';



export const ENTERED = 'ENTERED'
export function enterMainPage(page)
{
	return function(dispatch)
	{
		var entered = cookie.load("entered") || 0;
		if(entered)
			return;
		browserHistory.push(page);
		dispatch(setEntered());
		cookie.save("entered", true);
	}
}

export function setEntered() 
{
	return {
		type: ENTERED
	}
}



// The Reducer Function
const IntroReducer = function(state, action) 
{
	if (state === undefined) 
	{
		state = 
		{
			entered: false
		};
	}

	switch(action.type) 
	{
		case 'ENTERED':
			return Object.assign({}, state, { entered: true });
	}
	return state;
}

export default IntroReducer