import React from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux';
const RRedux = require('react-redux');
import ReactDOM from 'react-dom';
import thunkMiddleware from 'redux-thunk';

import { browserHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import './styles.less';



import AppRouter 			from './components/AppRouter';

import PageTitleReducer 	from './redux/reducers/PageTitleReducer';

import IntroReducer 		from './redux/reducers/IntroReducer';
import NavigationReducer 	from './redux/reducers/NavigationReducer';
import PortfolioReducer 	from './redux/reducers/PortfolioReducer';

import ProjectReducer	 	from './redux/reducers/ProjectReducer';
import AboutReducer	 		from './redux/reducers/AboutReducer';


const reducers = combineReducers(
{
	routing: 	routerReducer,
	
	PageTitle: 	PageTitleReducer,
	
	Intro: 		IntroReducer,
	Navigation: NavigationReducer,
	Portfolio: 	PortfolioReducer,
	
	Project: 	ProjectReducer,
	
	About:		AboutReducer
})

window.store = createStore(
	reducers, 
	applyMiddleware(thunkMiddleware)
);


const history = syncHistoryWithStore(browserHistory, store)


window.onload = () => 
{
	window.setTimeout(() => {
	ReactDOM.render(
		<RRedux.Provider store={store}>
			<AppRouter history={history}/>
		</RRedux.Provider>,

		document.getElementById('app')
	);
	}, 1000);
};
