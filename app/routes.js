// src/routes.js
import React from 'react'
import { Route, IndexRoute } from 'react-router'

import Layout from './components/Layout';
import Homepage from './components/Homepage';
import Portfolio from './components/Portfolio';
import About from './components/About';
import NotFoundPage from './components/NotFoundPage';


import Project from './components/projects/Project';



const routes = (
  <Route path="/" component={Layout}>
	<IndexRoute component={Homepage}/>
	<Route path="portfolio" component={Portfolio}>
		<Route path="*" component={Portfolio}>
			<Route path="*" component={Portfolio}/>
		</Route>
	</Route>
	<Route path="about" component={About}/>
	<Route path="*" component={NotFoundPage}/>
  </Route>
);

export default routes;