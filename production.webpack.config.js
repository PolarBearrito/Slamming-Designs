// In webpack.config.js
const webpack = require('webpack');
const path = require('path');
var moment = require("moment");

// Plugin to output a timestamp when compilation starts and ends
// Note that there's a one second delay before compiling due to the aggregateTimeout
function TimestampPlugin(options) {}

TimestampPlugin.prototype.apply = function (compiler)
{
    compiler.plugin('compile', function ()
    {
        // http://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
        console.log('\x1b[36m' + 'Started:\t' + new moment().format("ddd MMM D, YYYY, h:mm:ss a"));
    });

    compiler.plugin('done', function ()
    {
        console.log('\x1b[36m' + 'Ended:\t\t' + new moment().format("ddd MMM D, YYYY, h:mm:ss a"));
    });
};

// modified version of: http://stackoverflow.com/questions/39141089/in-webpack-watch-i-only-see-the-first-error
// Webpack CLI (command line interface) will only output the first error when compiling
// Subsequent compilations will not have error messages for any other errors, 
// there will just be a start and end timestamp, an instant compile

// This plugin will display subsequent errors on subsequent compiles,
// however, it will only display the first one, I think because webpack stops compiling
// after encountering an error
// but at least now you can see the others errors after you address previous errors
function ConsoleNotifierPlugin() { }

function log(error) 
{
	if(error.error) console.log(error.error.toString());
	else console.log(error.toString());
}

ConsoleNotifierPlugin.prototype.compilationDone = function (stats)
{
    stats.compilation.errors.forEach(log);
}

ConsoleNotifierPlugin.prototype.apply = function (compiler)
{
    compiler.plugin('done', this.compilationDone.bind(this))
}

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractLess = new ExtractTextPlugin
({
    filename: (getPath) => 
	{
      return getPath(path.join("..", "css", "styles.css"))
    }
});

module.exports = 
{
	entry: [
	'./app/index.js'
	],
	
	output: 
	{
		path: path.join(__dirname, "dist", "static", "js"),
		filename: "index_bundle.js"
	},
	
	module: 
	{
		rules: 
		[
			{
				test: /\.js$/, 
				include: path.join(__dirname, "app"), 
				exclude: /node_modules/,
				loader: "babel-loader",
				query: 
				{
					cacheDirectory: 'babel_cache',
					presets: ['react', 'es2015', 'stage-0'],
					forceEnv: 'website'
				}	
			},
			
			{
				test: /\.less$/,
				include: path.join(__dirname, "app"), 
				use: extractLess.extract(
				{
					use: 
					[
						{loader: "css-loader"}, 
						{loader: "less-loader"}
					]
				})
			}
		]
	},
	
	plugins: [
		new webpack.DefinePlugin(
		{	
			'process.env.NODE_ENV': JSON.stringify("production")
		}),
		new TimestampPlugin,
        new ConsoleNotifierPlugin(),
		new webpack.LoaderOptionsPlugin({    debug: true, sourceMap: false  }),
		new webpack.optimize.UglifyJsPlugin
		({
			compress: { warnings: false	},
			sourceMap: false
		}),
		extractLess
		
	],
	
	resolveLoader: 
	{
		modules: [path.join(__dirname, "node_modules")]
	}
};